--------------------------------------------------------------------------------------
-- A class for handling the end-of-game conditions for the 1v1 Solo Mid match type.
-- 
-- A hero is declared the winner if one of these conditions are met:
--      1) The hero kills the opposing hero twice 
--      2) The hero destroys the opposing team's first mid tower. 
--
-- @classmod EndOfGame.SoloMidWinCondition
-- @author Robert Smith
--------------------------------------------------------------------------------------

-- Create a table for holding all class features.
local SoloMidWinCondition = {}

-- Initialize any properties necessary for execution.
SoloMidWinCondition.RadiantKills = 0
SoloMidWinCondition.DireKills    = 0
SoloMidWinCondition.TowerKilled  = "None"
SoloMidWinCondition.Winner       = "None"

--------------------------------------------------------------------------------------
-- Function SoloMidWinCondition:CheckForVictory()
--
-- Update the state of the win condition object, then if any victory conditions 
-- are met, we return true. Otherwise return false.
--------------------------------------------------------------------------------------
function SoloMidWinCondition:CheckForVictory()
    -- Record the current kills for each team.
    self.RadiantKills = GetTeamHeroKills(2)
    self.DireKills    = GetTeamHeroKills(3)

    -- If Radiant has two kills, they win.
    if self.RadiantKills >= 2 then
        self.Winner = "Radiant"
        GameRules:SetGameWinner(2)
        do return true end
    end
    
    -- If Dire has two kills, they win.
    if self.DireKills >= 2 then
        self.Winner = "Dire"
        GameRules:SetGameWinner(3)
        do return true end
    end
    
    -- If the Dire tower was destroyed, Radiant wins.
    if not Entities:FindByName(nil, "dota_badguys_tower1_mid") then
        self.Winner      = "Radiant"
        self.TowerKilled = "Dire"
        GameRules:SetGameWinner(2)
        do return true end
    end
    
    -- If the Radiant tower was destroyed, Radiant wins.
    if not Entities:FindByName(nil, "dota_goodguys_tower1_mid") then
        self.Winner      = "Dire"
        self.TowerKilled = "Radiant"
        GameRules:SetGameWinner(3)
        do return true end
    end
    
    -- If none of these win conditions are true, return false.
    return false
end


-- Return the table for this action. This becomes
-- accessible on a "require" call to this file.
return SoloMidWinCondition
