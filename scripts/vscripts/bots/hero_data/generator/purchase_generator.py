import simplejson as json
from pprint import pprint

from data_handlers import *
from code_handler import GenerateCode

def Main():
    template = OpenTemplate("./purchaser.lua_t")
    files = GetJsonList("../json")

    for fileName in files:
        print("Generating purchaser file for " + fileName[:-5] + ":")
        file = open("../json/" + fileName)
        data = json.load(file)
        file.close()
        
        title  = data["R_HERO_TITLE_NAME"]
        titleR = title.replace(" ", "")
        name   = data["R_HERO_NAME"]
        items  = data["R_ITEM_LIST"]
        spells = data["R_ABILITY_LIST"]
        code   = data["R_MANAGE_INVENTORY_CODE"]

        itemCount = str(len(items))
        spellCount = str(len(spells))

        # Run the appropriate data handlers.
        items = ConvertListToString(items)
        spells = ConvertListToString(spells)

        # Create a local copy of the template data.
        output = str(template)
        
        # Replace all of the Lua codes with their
        # appropriate JSON-derived substitutions.
        print("\tReplacing R_HERO_TITLE_NAME_WSR with " + titleR + "...")
        output = output.replace("R_HERO_TITLE_NAME_WSR", titleR)

        print("\tReplacing R_HERO_TITLE_NAME with " + title + "...")
        output = output.replace("R_HERO_TITLE_NAME", title)

        print("\tReplacing R_HERO_NAME with " + name + "...")
        output = output.replace("R_HERO_NAME", name)

        print("\tReplacing R_ITEM_LIST with JSON items string...")
        output = output.replace("R_ITEM_LIST", items)

        print("\tReplacing R_ITEM_COUNT with " + itemCount + "...")
        output = output.replace("R_ITEM_COUNT", itemCount)
        
        print("\tReplacing R_ABILITY_LIST with JSON ability string...")
        output = output.replace("R_ABILITY_LIST", spells)

        print("\tReplacing R_ABILITY_COUNT with " + spellCount + "...")
        output = output.replace("R_ABILITY_COUNT", spellCount)

        # Replace the ManageInventory() code section with the appropriate
        # code generated from the JSON command list.
        print("\tReplacing R_MANAGE_INVENTORY_CODE with generated code...")
        output = output.replace("R_MANAGE_INVENTORY_CODE", GenerateCode(code))
        
        # Write the output contents to its appropriate purchaser file.
        print("\tWriting file '" + fileName[:-5] + ".lua'...")
        fileOut = open("../" + fileName[:-5] + ".lua", "w")
        fileOut.write(output)
        fileOut.close()
        print("\tComplete.\n")
    
if __name__ == "__main__":
    Main()
