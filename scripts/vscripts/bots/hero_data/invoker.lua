--------------------------------------------------------------------------------------
-- An interface for providing purchasing functionality for Invoker. 
--
-- @classmod Purchaser.Invoker
-- @author Robert Smith
--------------------------------------------------------------------------------------

-- Import any other required files.
local json = require("bots/lib/dkjson")

-- Create a table for holding all class features.
local Purchaser = {}

-- Initialize any properties necessary for execution.
Purchaser.HeroName           = "Invoker"
Purchaser.AbilityCount       = 25
Purchaser.ItemCount          = 27

Purchaser.ItemList           = {"item_flask", "item_tango", "item_tango", "item_branches", "item_sobi_mask", "item_circlet", "item_ring_of_protection", "item_recipe_urn_of_shadows", "item_boots", "item_magic_stick", "item_branches", "item_recipe_magic_wand", "item_chainmail", "item_blades_of_attack", "item_staff_of_wizardry", "item_void_stone", "item_wind_lace", "item_recipe_cyclone", "item_vitality_booster", "item_recipe_spirit_vessel", "item_point_booster", "item_staff_of_wizardry", "item_ogre_axe", "item_blade_of_alacrity", "item_mystic_staff", "item_ultimate_orb", "item_void_stone"}
Purchaser.AbilityList        = {"invoker_quas", "invoker_wex", "invoker_quas", "invoker_wex", "invoker_quas", "invoker_wex", "invoker_quas", "invoker_wex", "invoker_wex", "special_bonus_unique_invoker_10", "invoker_exort", "invoker_wex", "invoker_wex", "invoker_quas", "special_bonus_unique_invoker_9", "invoker_quas", "invoker_exort", "invoker_exort", "invoker_exort", "special_bonus_unique_invoker_5", "invoker_quas", "invoker_exort", "invoker_exort", "invoker_exort", "special_bonus_unique_invoker_2"}

Purchaser.CurrentItem        = 1
Purchaser.CurrentAbility     = 1

Purchaser.HeroHandler        = nil

--------------------------------------------------------------------------------------
-- Function Purchaser:ManageInventory()
--
-- Change the hero's inventory as necessary before an item purchase. This is
-- auto-generated from the purchaser generator script.
--------------------------------------------------------------------------------------
function Purchaser:ManageInventory()
	if self.HeroHandler:GetNumItemsInInventory() >= 6 then
		local item = self.HeroHandler:FindItemInInventory("item_tango")
		self.HeroHandler:RemoveItem(item)
	end

	if self.HeroHandler:GetNumItemsInInventory() >= 6 then
		local item = self.HeroHandler:FindItemInInventory("item_flask")
		self.HeroHandler:RemoveItem(item)
	end

	return true
end

--------------------------------------------------------------------------------------
-- Function Purchaser:SetHeroHandler(Unit)
--
-- Set the hero handler so we can make changes to a hero unit directly later.
--------------------------------------------------------------------------------------
function Purchaser:SetHeroHandler(Unit)
    self.HeroHandler = Unit
end

--------------------------------------------------------------------------------------
-- Function Purchaser:BuyNextAbility()
--
-- Attempt to buy the next ability in the hero's ability list.
--------------------------------------------------------------------------------------
function Purchaser:BuyNextAbility()
    -- If we haven't set a hero handler, we fail negatively.
    if self.HeroHandler == nil then
        do return -1 end
    end
    
    -- If we've already purchased all abilities and talents, we fail positively.
    if self.CurrentAbility > self.AbilityCount then
        do return 2 end
    end
        
    -- If we don't have enough ability upgrade points, we fail negatively.
    if self.HeroHandler:GetAbilityPoints() < 1 then
        do return -2 end
    end
        
    -- Since we haven't failed, we start by getting the ability from the handler.
    local ability = self.HeroHandler:FindAbilityByName(self.AbilityList[self.CurrentAbility])
    
    -- If we couldn't find the ability, we fail negatively.
    if ability == nil then
        do return -3 end
    end
    
    -- We found the ability, so we can upgrade it by one level.
    self.HeroHandler:UpgradeAbility(ability)
    
    -- Increase the current ability counter by 1.
    self.CurrentAbility = self.CurrentAbility + 1

    -- Return positively.
    return 1
end

--------------------------------------------------------------------------------------
-- Function Purchaser:BuyNextItem()
--
-- Attempt to buy the next item in the hero's ability list.
--------------------------------------------------------------------------------------
function Purchaser:BuyNextItem()
    -- If we haven't set a hero handler, we fail negatively.
    if self.HeroHandler == nil then
        do return -1 end
    end

    -- If we've already purchased all items, we fail positively.
    if self.CurrentItem > self.ItemCount then
        do return 2 end
    end

     -- Get the name of the item to be purchased.
    local item = self.ItemList[self.CurrentItem]
    
    -- Get the cost of the item to be purchased.
    local cost = GetItemCost(item)

    -- If we can't afford the next item, we fail negatively.
    if cost > self.HeroHandler:GetGold() then
        do return -2 end
    end

    -- Start by managing the hero's inventory as necessary.
    -- Most functions after this function assumes the hero's
    -- inventory has been properly set up with this function.
    self:ManageInventory()    
    
    -- If the hero still doesn't have space for the item, we fail negatively.
    -- If the item is a recipe, we don't need to follow this rule, as the
    -- inventory should not be larger after purchase unless something
    -- weird is going on.
    if self.HeroHandler:GetNumItemsInInventory() > 5 and 
       not string.find(self.ItemList[self.CurrentItem], "recipe") then
        do return -3 end
    end
       
    -- Spend the required gold to purchase the item.
    self.HeroHandler:SpendGold(cost, DOTA_ModifyGold_PurchaseItem)
    
    -- Add the item to the hero's inventory.
    self.HeroHandler:AddItemByName(item)
      
    -- Increase the current item counter by 1.
    self.CurrentItem = self.CurrentItem + 1

    -- Return positively.
    return 1
end

--------------------------------------------------------------------------------------
-- Function Purchaser:GetItemPurchaseHistory()
--
-- Return a table of the hero's previously purchased items in order.
--------------------------------------------------------------------------------------
function Purchaser:GetItemPurchaseHistory()
    -- Create a local history table.    
    local history = {}
 
    -- Get every item from the Item List that has been 
    -- purchased so far and add it to the history table.
    for i=1, self.CurrentItem-1, 1 do
        history[i] = self.ItemList[i]
    end
    
    -- Return the item history table.
    return history
end

--------------------------------------------------------------------------------------
-- Function Purchaser:GetAbilityPurchaseHistory()
--
-- Return a table of the hero's previously purchased abilities in order.
--------------------------------------------------------------------------------------
function Purchaser:GetAbilityPurchaseHistory()
    -- Create a local history table.    
    local history = {}
 
    -- Get every ability from the Ability List that has been 
    -- purchased so far and add it to the history table.
    for i=1, self.CurrentAbility-1, 1 do
        history[i] = self.AbilityList[i]
    end
    
    -- Return the item history table.
    return history
end

--------------------------------------------------------------------------------------
-- Function Purchaser:GetStateAsTable()
--
-- Return a table of the Purchaser's current state.
--------------------------------------------------------------------------------------
function Purchaser:GetStateAsTable()
    -- Create a table to hold state.
    local state = {}
    
    -- Save the hero name to the state.
    state.HeroName           = self.HeroName
    
    -- Save the next item and ability purchases to the state.
    state.CurrentItem        = self.ItemList[self.CurrentItem]
    state.CurrentAbility     = self.AbilityList[self.CurrentAbility]
    
    -- Save the last item and ability purchases to the state.
    state.LastItem           = self.ItemList[self.CurrentItem-1]
    state.LastAbility        = self.AbilityList[self.CurrentAbility-1]
    
    -- Save the number of items and abilities purchased to the state.
    state.ItemsPurchased     = self.CurrentItem-1
    state.AbilitiesPurchased = self.CurrentAbility-1
    
    -- Return the state table.
    return state
end

--------------------------------------------------------------------------------------
-- Function Purchaser:PrintState()
--
-- Print the state of the Purchaser.
--------------------------------------------------------------------------------------
function Purchaser:PrintState()
    print(json.encode(self.GetStateAsTable()))
end

-- Return the table for this action. This becomes
-- accessible on a "require" call to this file.
return Purchaser
