--------------------------------------------------------------------------------
-- Create a string utility table for adding extra string functions.
--
-- @classmod Utility.StringUtil
-- @author Robert Smith
--------------------------------------------------------------------------------

local StringUtil = {}

--------------------------------------------------------------------------------
-- Function StringUtil:Split(InputString, Separator)
--
-- Splits the input string into tokens by using the separator as a delimiter.
-- 
-- Based on: https://stackoverflow.com/questions/1426954/split-string-in-lua
--------------------------------------------------------------------------------
function StringUtil:Split(input, separator)
    assert(type(input) == "string", "String:Split(): First parameter must be a string.")

    -- If the separator is nil or empty, then the separator is the string.
    if not separator then
        separator = "%s"
    end
    
    -- Create a new table for holding the split token strings.
    local tokens = {}
    
    -- Split the input string into tokens delimited by the 
    -- separator. Once the tokens are generated, add them
    -- all to the token table. 
    for str in string.gmatch(input, "([^"..separator.."]+)") do
        table.insert(tokens, str)
    end
    
    -- Return the token table.
    return tokens
end

--------------------------------------------------------------------------------
-- Function StringUtil:HashCode(InputString)
--
-- Generates a numerical hashcode for the input string.
--------------------------------------------------------------------------------
function StringUtil:HashCode(input)
    assert(type(input) == "string", "String:HashCode(): First parameter must be a string.")

    -- Create a starting hash code value.
    local hash = 7;
    
    -- For every character in the string, run the
    -- hash formula and add its value to the hash.
    for i=0, input:len() do
        hash = hash * 31 + string.byte(input:sub(i)) 
    end
    
    -- Return the hash value.
    return hash
end

return StringUtil