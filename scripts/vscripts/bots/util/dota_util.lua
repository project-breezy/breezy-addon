--------------------------------------------------------------------------------
-- Create an object for holding a multitude of Dota 2 related utility functions.
--
-- @classmod Utility.Dota2Util
-- @author Robert Smith
--------------------------------------------------------------------------------

-- Import any required code.
require("bots/util/sys_util")

-- Create a table for holding action properties, as
-- well as the main function call logic.
local Dota2Util = {}

-- Initialize any properties necessary for execution.
Dota2Util.RuneTypes = {
    goldxp       = 5,    
    doubledamage = 0,  
    haste        = 1,
    illusion     = 2,
    invisibility = 3,
    regeneration = 4,
    arcane       = 6
}

--------------------------------------------------------------------------------
-- Function CastOnLocation:ConvertToCartesianInVision(Unit, degrees, radius)
--
-- Convert the degrees and radius to x,y coordinates on a cartesian plane,
-- which should be limited to points only accessible in the unit's vision.
--------------------------------------------------------------------------------
function Dota2Util:ConvertToCartesianInVision(Unit, degrees, radius)
    -- Convert the incoming angle degrees to radians.
    local theta  = degrees * math.pi/180
    
    -- Get the unit's current vision range.
    local vision = Unit:GetCurrentVisionRange()
    
    -- Limit the radius with a mod by the maximum, then use that value
    -- to convert the polar coordinates to cartesian coordinates.    
    local x = (radius % vision) * math.cos(theta)
    local y = (radius % vision) * math.sin(theta)
    
    -- Add the Unit's current location, as this movement is relative.
    local location = Unit:GetAbsOrigin()
    x = x + location[1]
    y = y + location[2]
    
    -- Return the cartesian coordinate values.
    return x, y    
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetHeroesOnTeam(TeamIndex)
--
-- Return a table of all heroes that belong to the given team.
--------------------------------------------------------------------------------
function Dota2Util:GetHeroesOnTeam(team)
    -- Use HeroList from Valve API to get all heroes in the current match.
    local heroTable = HeroList:GetAllHeroes()
    
    -- Create an empty table for holding the necessary heroes.
    local result = {}
    
    -- Iterate through all the found tables.
    for key, hero in pairs(heroTable) do
        -- If the hero belongs to the designated team,
        -- store it in the result table.
        if hero:GetTeam() == team then
            table.insert(result, hero)
        end
    end
    
    -- Return the constructed list of heroes.
    return result
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetNearbyLaneCreeps(OriginVector, Radius, TeamIndex)
--
-- Return a table of all creeps nearby who belong to the given team.
--------------------------------------------------------------------------------
function Dota2Util:GetNearbyLaneCreeps(origin, radius, team)
    -- Get a table of all creeps of the given team within the vision range
    -- of the current unit. The table is sorted by closest to farthest
    -- creep from the current unit. Return the filled table.
    return FindUnitsInRadius(team,
                             origin,
                             nil,
                             radius,
                             DOTA_UNIT_TARGET_TEAM_FRIENDLY,
                             DOTA_UNIT_TARGET_CREEP,
                             DOTA_UNIT_TARGET_FLAG_NONE,
                             FIND_CLOSEST,
                             false)
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetItemFromUnitByName(Unit, ItemName, Index1, Index2)
--
-- Iterate through the unit's inventory slots [Index1, Index2] and
-- return the handler to the item with the name matching ItemName.
--------------------------------------------------------------------------------
function Dota2Util:GetItemFromUnitByName(Unit, name, first, last)
    -- If the first index is larger than the last index, return negatively.
    if first > last then
        do return -1 end
    end
    
    -- If the first index is out of range, return negatively.
    if first < 0 or first > 8 then
        do return -2 end
    end
    
    -- If the last index is out of range, return negatively.
    if last < 0 or last > 8 then
        do return -3 end
    end
        
    -- If the unit has the item in the inventory, proceed.    
    if Unit:HasItemInInventory(name) then
        -- Iterate through the inventory (and/or backpack).
        for index=first, last do
            -- Get the item handler from the current index.
            local tempItem = Unit:GetItemInSlot(index)
            
            -- If the item exists and item's name matches 
            -- the item we want, return that item handler.
            if tempItem and tempItem:GetAbilityName() == name then
                do return tempItem end
            end
        end
    end
    
    -- Return false if the item isn't in the unit's inventory.
    return false
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetItemFromInventoryByName(Unit, ItemName)
--
-- Search through the unit's main inventory (not backpack) for the item
-- matching the ItemName and return its handler.
--------------------------------------------------------------------------------
function Dota2Util:GetItemFromInventoryByName(Unit, name)
    return self:GetItemFromUnitByName(Unit, name, 0, 5)
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetItemFromBackpackByName(Unit, ItemName)
--
-- Search through the unit's backpack (not main inventory) for the item
-- matching the ItemName and return its handler.
--------------------------------------------------------------------------------
function Dota2Util:GetItemFromBackpackByName(Unit, name)
    return self:GetItemFromUnitByName(Unit, name, 6, 8)
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetItemFromFullInventoryByName(Unit, ItemName, I1, I2)
--
-- Search through the unit's inventory and backpack for the item named
-- ItemName and return its handler.
--------------------------------------------------------------------------------
function Dota2Util:GetItemFromFullInventoryByName(Unit, name)
    return self:GetItemFromUnitByName(Unit, name, 0, 8)
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetItemSlot(Unit, ItemName)
--
-- Search through all items in the full inventory for the item matching
-- the ItemName, then return the inventory slot in which it was found.
-- This function will always find the lowest slot number for the item if
-- the item appears more than once in the unit's inventory.
--------------------------------------------------------------------------------
function Dota2Util:GetItemSlot(Unit, name)
    -- Search for the named item in the unit's inventory.
    local item = self:GetItemFromUnitByName(Unit, name, 0, 8)
    
    -- If that inventory is in the unit's inventory, return
    -- its assigned slot index value.
    if item then
        do return item:GetItemSlot() end
    end
    
    -- If the item was not found, return false.
    return false
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetRuneTypeInformation(Rune)
--
-- Determine the type code and name of the rune and return it as {name, code}.
--------------------------------------------------------------------------------
function Dota2Util:GetRuneTypeInformation(Rune)
    -- If the rune is nil, return an invalid rune.
    if not Rune then
        return {nil, -1}
    end
    
    -- Extract the model path string from the rune handler.
    local runeName = Rune:GetModelName()
    
    -- Some of the model names have a "01" token on the end, before 
    -- the file extension. If this model name has a "01" token 
    -- inside, then we account for it here in the offset.
    local offset = string.find(runeName, "0", string.len(runeName)-7) and 7 or 5 
    
    -- All model paths start with 28 characters of useless data 
    -- and end with either 5 or 7 characters of useless data.
    runeName = string.sub(runeName, 28, string.len(runeName)-offset)
       
    -- Check if the model name is present in the rune type table.
    local runeCode = self.RuneTypes[runeName]
    
    -- If the runeCode exists in the table, return it and the name.
    if runeCode then
        do return {runeName, runeCode} end
    end   
    
    -- If we have found a rune that is invalid, return a -1
    -- and set the return name to nil.
    return {nil, -1}
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetRuneName(Rune)
--
-- Determine the name of the rune and return it.
--------------------------------------------------------------------------------
function Dota2Util:GetRuneName(Rune)
    return self:GetRuneTypeInformation(Rune)[1] or false
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetRuneType(Rune)
--
-- Determine the type code of the rune and return it.
--------------------------------------------------------------------------------
function Dota2Util:GetRuneType(Rune)
    return self:GetRuneTypeInformation(Rune)[2] or false
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetAllRunes()
--
-- Search the map for all currently spawned runes and return them in a table.
--------------------------------------------------------------------------------
function Dota2Util:GetAllRunes()
    -- Get all currently spawned runes from the map.
    local runes = Entities:FindAllByClassname("dota_item_rune")
    
    -- Return the table of runes if there were any.
    if size(runes) > 0 then
        do return runes end
    end
    
    -- Return an empty table if there are no runes.
    return {}
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetAllRunesWithTypeSplit()
--
-- Search the map for all currently spawned runes, then divide them into
-- two tables (bounty and power), then return both.
--------------------------------------------------------------------------------
function Dota2Util:GetAllRunesWithTypeSplit()
    -- Get all currently spawned runes from the map.
    local runes = Entities:FindAllByClassname("dota_item_rune")
    
    -- Create tables for power and bounty runes.
    local power  = {}
    local bounty = {}
    
    -- Iterate through all of the discovered runes.
    for key, rune in pairs(runes) do
        -- If the rune is a bounty rune, add it to the bounty 
        -- table. Otherwise add it to the power table.
        if self:GetRuneType(rune) == DOTA_RUNE_BOUNTY then
            table.insert(bounty, rune)
        else
            table.insert(power, rune)
        end
    end
    
    -- Return the table of runes if there were any.
    if size(runes) > 0 then
        do return runes end
    end
    
    -- Return each table. If a table is empty, return an empty table.
    return size(bounty) > 0 and bounty or {}, 
           size(power) > 0 and power or {} 
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetAllBountyRunes()
--
-- Search the map for all bounty runes and return them as a table.
--------------------------------------------------------------------------------
function Dota2Util:GetAllBountyRunes()
    local bounty, power = self:GetAllRunesWithTypeSplit()
    return bounty
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetAllPowerRunes()
--
-- Search the map for all powerup runes and return them as a table.
--------------------------------------------------------------------------------
function Dota2Util:GetAllPowerRunes()
    local bounty, power = self:GetAllRunesWithTypeSplit()
    return power
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetBountyRuneByIndex(Index)
--
-- Get the Index-th bounty rune on the map. The index for a given
-- bounty rune is likely to differ from patch-to-patch.
--------------------------------------------------------------------------------
function Dota2Util:GetBountyRuneByIndex(index)
    -- Get a table of all bounty runes.
    local runes = self:GetAllBountyRunes()
    
    -- If the bounty runes exist, get the rune at the given
    -- index. If that rune doesn't exist, return false. 
    if runes then
        do return runes[index] or false end
    end
    
    -- If we weren't able to find any runes, return false.
    return false
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetRuneStatus(Unit, RuneSpawner)
--
-- If the rune can be seen/identified by the given unit, return a 1.
-- If the rune location is visible, but there's no rune, return a -1.
-- If the rune location cannot be seen by the unit, return a 0. 
--------------------------------------------------------------------------------
function Dota2Util:GetRuneStatusByUnit(Unit, Rune)
    -- If the rune is nil, we return -1.
    -- TODO: This is potentially more knowledge than the unit has. Not good.
    if not Rune then
        do return -1 end
    end
    
    -- If we can't see the rune location, then we can return 0 
    -- because we don't know the rune status.
    if not IsLocationVisible(Unit:GetTeam(), Rune:GetAbsOrigin()) then
        do return 0 end
    end
    
    -- Since the location is visible, find all runes near the spawner location.
    local entities = FindAllByClassnameWithin("dota_item_rune", Spawner:GetAbsOrigin(), 100)
    
    -- If no runes are found at the visible location, we return -1.
    if not entities or size(entities) == 0 then
        do return -1 end
    end 
    
    -- If we found a rune (and there should only be one), we return 1.
    return 1
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetBaseDamageVariance(Unit)
--
-- Return the variance of the given unit's attack damage.
-- 
-- Note: For the data set x, variance of a population is calculated as:
--     S^2 = (Sum (x_i - x_mean)^2) / x.size 
--------------------------------------------------------------------------------
function Dota2Util:GetBaseDamageVariance(Unit)
    -- Extract the minimum and maximum attack damage from the unit.
    local minDamage = Unit:GetBaseDamageMin()
    local maxDamage = Unit:GetBaseDamageMax()
    
    -- Calculate the average damage for the unit.
    local avgDamage = (minDamage + maxDamage)/2

    -- Calculate the square distance for the two data points.
    local deltaMin = (minDamage - avgDamage)^2
    local deltaMax = (maxDamage - avgDamage)^2

    -- Find the sum average of the square distances.
    local variance = (deltaMin + deltaMax)/2

    -- Return the variance.
    return variance
end

--------------------------------------------------------------------------------
-- Function Dota2Util:GetNetWorthByUnit(Unit)
--
-- Return the net worth of the given unit.
--------------------------------------------------------------------------------
function Dota2Util:GetNetWorthByUnit(Unit)
    return PlayerResource:GetNetWorth(Unit:GetPlayerID())
end

return Dota2Util