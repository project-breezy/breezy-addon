--------------------------------------------------------------------------------
-- Creates a table for holding additional system functionality and shortcuts.
--
-- @classmod Utility.SysUtil
-- @author Robert Smith
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Function confine(value, maximum, minimum)
--
-- Perform simple arithmetic such that the value parameter will be converted
-- to a value in the range [minimum, maximum]. Outputs are always integers.
-- 
-- If the maximum and minimum are missing
--------------------------------------------------------------------------------
function confine(value, minimum, maximum, T)
    -- If minimum is nil, then minimum is set to 0.
    if minimum == nil then
        minimum = 0
    end
    
    -- If maximum is nil, then maximum is set one higher than the minimum.
    if maximum == nil then
        maximum = minimum + 1
    end    
    
    -- If all of the values aren't numbers, we stop by assert.
    assert(value ~= nil,              "confine(): first parameter has no value.")
    assert(type(value)   == "number", "confine(): first parameter not a number.")
    assert(type(maximum) == "number", "confine(): second parameter not a number.")
    assert(type(minimum) == "number", "confine(): third parameter not a number.")

    -- Decrease the value by the minimum.
    local newValue = value - minimum

    -- Calculate the range.
    local range = maximum - minimum + 1

    -- Put the value parameter in the range with modulus and add the minimum.
    local confined = minimum + (newValue % range)
    
    -- Return the floor of the confined value.
    return math.floor(confined)
end

--------------------------------------------------------------------------------
-- Function size(Table)
--
-- Count the number of things in the table X, regardless of the key types.
-- If X is not a table or is nil, we returns false.
--------------------------------------------------------------------------------
function size(X)
    -- If X is nil or X is not a table, return false.
    if X == nil or type(X) ~= "table" then
        do return false end
    end
    
    -- Create an integer counter.
    local count = 0
    
    -- Count every K->V pair in X.
    for K,V in pairs(X) do count = count + 1 end
    
    -- Return the number of elements.
    return count
end

-- TODO: Get this going.
function uuid()

end
