--------------------------------------------------------------------------------
-- Creates a table for containing the Action Handler logic.
--
-- The Action Handler will accept a table of real values (float/double) and use
-- them to determine an action which can be applied to the Dota 2 game world.
-- 
-- The first value determines the type of action to apply, and the remaining
-- values will be used to fill in the various required values to perform the
-- chosen action. 
-- 
-- Actions have a certain parameter requirement and only the necessary values
-- will be provided to each individual action class as necessary. All other
-- values will be discard. Thus, an agent must always attempt generate the
-- largest required number of actions on every request.
--
-- @classmod ActionManager
-- @author Robert Smith
--------------------------------------------------------------------------------

-- Import any required code.
require("bots/util/sys_util")

-- Create any required constants.
DO_NOTHING       = 0
MOVE_TO_LOCATION = 1
ATTACK_HERO      = 2
ATTACK_TOWER     = 3
ATTACK_CREEP     = 4
CAST_ON_LOCATION = 5
CAST_ON_UNIT     = 6
USE_ITEM         = 7

-- Create a table for holding action properties, as
-- well as the main function call logic.
local ActionHandler = {}
ActionHandler.__index = ActionHandler

-- Initialize any properties necessary for execution.
ActionHandler.NumberOfActions = 8
ActionHandler.RequiredValues  = 5
ActionHandler.Debug           = true

-- Initialize the action table to match the constants above.
ActionHandler.ActionTable = {
    require("bots/action_handler/actions/MoveToLocation"),
    require("bots/action_handler/actions/AttackHero"),
    require("bots/action_handler/actions/AttackTower"),
    require("bots/action_handler/actions/AttackCreep"),
    require("bots/action_handler/actions/CastOnLocation"),
    require("bots/action_handler/actions/CastOnUnit"),
    require("bots/action_handler/actions/UseItem")
}

-- Initialize the 0 key separately, as the table constructor can't do it.
ActionHandler.ActionTable[0] = require("bots/action_handler/actions/DoNothing")

--------------------------------------------------------------------------------
-- Function ActionHandler:CreateNew(Parameters)
--
-- Accepts a table of parameters and creates a new ActionHandler object.
--------------------------------------------------------------------------------
function ActionHandler:CreateNew(parameters)
    -- Create a new object. 
    local newObject = {}
    
    -- Set the default table lookup location.
    setmetatable(newObject, ActionHandler)
    
    -- Initialize the new object with the input parameters table.
    newObject:Initialize(parameters)
    
    -- Return the new object.
    return newObject
end

--------------------------------------------------------------------------------
-- Function ActionHandler:CreateNew(Parameters)
--
-- Accepts a table of parameters and alters the relevant class variables.
--------------------------------------------------------------------------------
function ActionHandler:Initialize(parameters)
    -- Create a new action table.
    self.ActionTable = {}
    
    -- Populate the table using the parameters.
    self.ActionTable["DO_NOTHING"]       = require(parameters["DO_NOTHING"])
    self.ActionTable["MOVE_TO_LOCATION"] = require(parameters["MOVE_TO_LOCATION"])
    self.ActionTable["ATTACK_HERO"]      = require(parameters["ATTACK_HERO"])
    self.ActionTable["ATTACK_TOWER"]     = require(parameters["ATTACK_TOWER"])
    self.ActionTable["ATTACK_CREEP"]     = require(parameters["ATTACK_CREEP"])
    self.ActionTable["CAST_ON_LOCATION"] = require(parameters["CAST_ON_LOCATION"])
    self.ActionTable["CAST_ON_UNIT"]     = require(parameters["CAST_ON_UNIT"])
    self.ActionTable["USE_ITEM"]         = require(parameters["USE_ITEM"])
    
    -- Set the number of actions to 8.
    self.NumberOfActions = 8
    
    -- Start the required values at 0.
    self.RequiredValues = 0
    
    -- Set the required number of values for action tables.
    for index, action in pairs(self.ActionTable) do
        if action.RequiredValues > self.RequiredValues then
            self.RequiredValues = action.RequiredValues
        end
    end
    
    -- Add one to the required values to account for the action choice value.
    self.RequiredValues = self.RequiredValues + 1
    
    -- Set the debug mode.
    self.Debug = parameters["Debug"]
end

--------------------------------------------------------------------------------
-- Function ActionHandler:HandleAction(Parameters)
--
-- Accepts a table of action values and uses the unit to perform the 
-- relevant action in the game world.
--------------------------------------------------------------------------------
function ActionHandler:HandleAction(Unit, actions, thinkCount)
    -- If the actions are nil, return negatively.
    if not actions then
        print("ACTION_HANDLER: No Actions Received.")
        do return -1 end
    end
    
    -- If there are not enough action values, return negatively.
    if size(actions) ~= self.RequiredValues then
        print("ACTION_HANDLER: Action count incorrect.")
        print("ACTION_HANDLER: Expected " .. self.RequiredValues)
        print("ACTION_HANDLER: Got      " .. size(actions) or "Not a Table")
        do return -2 end
    end
    
    -- Extract the action choice from the action table.
    local choice = confine(actions[1], 0, self.NumberOfActions-1)

    -- Get the chosen action class from the action table.
    local Action = self.ActionTable[choice]
    
    -- Print the action type name.
    if choice ~= 0 then    
        print("Action " .. thinkCount .. ":", Action.Name)
    end
    
    -- Run the action depending on how many input parameters are necessary.
    -- Note that in the base action structure, the only Act() action input lengths are 0, 2, 3, and 4.
    if     Action.RequiredValues == 0 then Action:Act()
    elseif Action.RequiredValues == 2 then Action:Act(Unit, actions[2], actions[3], self.Debug)
    elseif Action.RequiredValues == 3 then Action:Act(Unit, actions[2], actions[3], actions[4], self.Debug)
    elseif Action.RequiredValues == 4 then Action:Act(Unit, actions[2], actions[3], actions[4], actions[5], self.Debug)
    end
    
    -- Return true if we successfully made it to the end of the HandleAction() function.
    return true
end

-- Return the table for this action. This becomes
-- accessible on a "require" call to this file.
return ActionHandler
