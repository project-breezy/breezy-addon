--------------------------------------------------------------------------------
-- Creates a table for containing the Move to Location function and its 
-- important parameters.
--
-- The Move to Location action is the same as a player right-clicking on the 
-- ground. The unit will use regular Dota 2 pathing mechanisms to reach the 
-- selected location.
--
-- @classmod Action.MoveToLocation
-- @author Robert Smith
--------------------------------------------------------------------------------

-- Import any required code.
local Dota2 = require("bots/util/dota_util")

-- Create a table for holding action properties, as
-- well as the main function call logic.
local MoveToLocation = {}

-- Initialize any properties necessary for execution.
MoveToLocation.Name           = "Move to Location"
MoveToLocation.RequiredValues = 2
MoveToLocation.MapBottomLeft  = { x=-8000, y=-8000 }
MoveToLocation.MapTopRight    = { x= 7900, y= 7400 }
MoveToLocation.XRange         = MoveToLocation.MapTopRight["x"] - 
                                MoveToLocation.MapBottomLeft["x"]
MoveToLocation.YRange         = MoveToLocation.MapTopRight["y"] - 
                                MoveToLocation.MapBottomLeft["y"]

--------------------------------------------------------------------------------
-- Function MoveToLocation:Act(Unit, Degrees, Radius, DebugFlag) 
--
-- Accepts a Dota unit and moves it to the x, y coordinates generated.
--------------------------------------------------------------------------------
function MoveToLocation:Act(Unit, degrees, radius, debug)
    -- If the debug parameter is nil or false, set it to false.
    if not debug then
        debug = false
    end

    -- Generate the cartesion coordinates from the polar coordinates.
    -- All x and y values will be found in the unit's vision.
    local x, y = Dota2:ConvertToCartesianInVision(Unit, degrees, radius)
    
    -- For the purposes of clicking the ground exactly, 
    -- find the ground height at (x,y).
    local z = GetGroundHeight(Vector(x, y, 0), nil)
    
    -- Construct the target location vector.
    local location = Vector(x, y, z)
    
    -- If we're going to draw debug visuals or execute 
    -- debug code blocks, we can do so here.
    if debug then
        -- Draw a blue circle at the selected move location for 1 frame.
        -- Parameters: (center, vRgb, a, rad, ztest, duration)
        DebugDrawCircle(location, Vector(0, 0, 255), 1.0, 150, false, 1)
        
        -- Draw a blue line from the unit to the move location for 1 frame.
        -- Parameters: (origin, target, r, g, b, ztest, duration)
        DebugDrawLine(Unit:GetAbsOrigin(), location, 0, 0, 255, false, 1)
    end
    
    -- Print the relevant parameters.
    print("\tLocation (x,y):", x, y)

    -- Move the unit to the target location.
    Unit:MoveToPosition(location)    
end

-- Return the table for this action. This becomes
-- accessible on a "require" call to this file.
return MoveToLocation