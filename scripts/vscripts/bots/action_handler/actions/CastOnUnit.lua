--------------------------------------------------------------------------------
-- Creates a table for containing the Cast on Location function and its 
-- important parameters.
--
-- The Cast on Unit action is the same as a player activating a spell and 
-- left-clicking on a unit. The unit will use regular Dota 2 pathing 
-- mechanisms to get within range of the target unit, at which point they will
-- cast the spell on that targeted unit.
--
-- @classmod Action.CastOnUnit
-- @author Robert Smith
--------------------------------------------------------------------------------

-- Import any required code.
local bit = require("bots/lib/numberlua")
require("bots/util/sys_util")

-- Create a table for holding action properties, as
-- well as the main function call logic.
local CastOnUnit = {}

-- Initialize any properties necessary for execution.
CastOnUnit.Name           = "Cast on Unit"
CastOnUnit.RequiredValues = 3

--------------------------------------------------------------------------------
-- Function CastOnUnit:Act(Unit, Team, TargetIndex, AbilityIndex, DebugFlag) 
--
-- Accepts a Dota unit and moves it close enough to the unit's coordinates so
-- that it can cast the spell defined by the spell index on the target unit.
--------------------------------------------------------------------------------
function CastOnUnit:Act(Unit, team, targetIndex, abilityIndex, debug)
    -- If the debug parameter is nil or false, set it to false.
    if not debug then
        debug = false
    end
    
    -- Create a table for holding all found abilities
    local abilities = {}

    -- Search all the unit's abilities for valid abilities and save them
    for i=0, Unit:GetAbilityCount()-1 do
        -- Grab the next ability in the unit's abilities list.
        local A = Unit:GetAbilityByIndex(i)
        
        -- We don't want to do any work if A is nil, because it will crash.
        -- Lua, very stupidly, does not have a continue keyword. Awesome.
        if A ~= nil and type(A) == "table" then
            -- As long as a player could see/use the ability and it's not
            -- passive, then we can add it to our list of valid abilities.
            if not A:IsHidden() and A:IsPassive() then
                table.insert(abilities, A)        
            end
        end
    end
       
    -- Convert the abilityIndex parameter into a legitimate index
    -- based on the number of abilities possessed by the unit.
    abilityIndex = confine(abilityIndex, 1, size(abilities))
    
    -- Get the current unit's vision range.
    local vision = Unit:GetCurrentVisionRange()
    
    -- Convert the incoming team parameter to an integer in [2,4].
    -- This is not strong for converting to a team identifier value because it
    -- assumes uniformity of the incoming team parameter value.
    team = confine(team, 2, 4)
    
    -- Get a table of all units of the given team within the vision range
    -- of the current unit. The table is sorted by closest to farthest
    -- unit from the current unit.
    local units = FindUnitsInRadius(team,
                                    Unit:GetAbsOrigin(),
                                    nil,
                                    vision,
                                    DOTA_UNIT_TARGET_TEAM_FRIENDLY,
                                    DOTA_UNIT_TARGET_ALL,
                                    DOTA_UNIT_TARGET_FLAG_NONE,
                                    FIND_CLOSEST,
                                    false)
    
    -- Get the unit handler for the provided index. 
    -- Note that this method is not particularly strong for converting
    -- incoming real values into indexes, as it assumes there's some
    -- uniformity to the targetIndex parameter. If there's not, then
    -- the bias in the values will create the same bias in the indexes.
    -- Also, since the number of creeps in range of the FindUnitsInRadius
    -- function will fluctuate, the size of the table will be biased.
    local target = nil
    if units ~= nil then
        local index = confine(targetIndex, 2, size(units))
        target = units[index]
    end
    
    -- If the target is still nil, we can simply do nothing and return.
    if target == nil then 
        print("\tNo Valid Target Found")
        do return end
    end
        
    -- Get the ability handler from the abilities table.
    local ability = abilities[abilityIndex]
    
    -- Print the relevant parameters.
    print("\tAbility Name:", ability:GetName())
    print("\tAbility State:", ability:IsFullyCastable() and "Ready" or "On Cooldown/Unlearned")
    print("\tTarget:", target:GetName())    
        
    -- Determine if the selected ability needs a unit target.    
    local needsTarget = bit.band(ability:GetBehavior(), DOTA_ABILITY_BEHAVIOR_UNIT_TARGET) 
        
    -- If the ability cannot target a unit, we can stop.
    if needsTarget ~= DOTA_ABILITY_BEHAVIOR_UNIT_TARGET then
        print("\tCannot be cast on target")
        do return end
    end    
    
    -- If we're going to draw debug visuals or execute 
    -- debug code blocks, we can do so here.
    if debug then
        -- Get the location vector for the target unit.
        local location = target:GetAbsOrigin()
        
        -- Draw a blue circle at the selected move location for 1 frame.
        -- Parameters: (center, vRgb, a, rad, ztest, duration)
        DebugDrawCircle(location, Vector(0, 0, 255), 1.0, 150, false, 1)
        
        -- Draw a blue line from the unit to the move location for 1 frame.
        -- Parameters: (origin, target, r, g, b, ztest, duration)
        DebugDrawLine(Unit:GetAbsOrigin(), location, 0, 0, 255, false, 1)
    end
  
    -- If the ability is on cooldown, we don't bother trying to cast
    -- it and return negatively.
    if not ability:IsFullyCastable() then
        do return -2 end
    end
    
    -- Cast the ability on the target unit. No idea what the 0 is for.
    Unit:CastAbilityOnTarget(target, ability, 0)
end

-- Return the table for this action. This becomes
-- accessible on a "require" call to this file.
return CastOnUnit