--------------------------------------------------------------------------------
-- Creates a table for containing the Use Item function and 
-- its important parameters.
--
-- The Use Item action allows a unit to use an item. If the item must be cast
-- on the unit, it does so automatically. If the item must be activated on the
-- ground, it does so based on a provided x, y coordinate. If the item must
-- be activated on a unit, it will scan the visibility range of the unit
-- and cast on a selected unit within range of the item. 
--
-- @classmod Action.UseItem
-- @author Robert Smith
--------------------------------------------------------------------------------

-- Import any required code.
local Dota2 = require("bots/util/dota_util")
local bit   = require("bots/lib/numberlua")
require("bots/util/sys_util")

-- Create a table for holding action properties, as
-- well as the main function call logic.
local UseItem = {}

-- Initialize any properties necessary for execution.
UseItem.Name           = "Use Item"
UseItem.RequiredValues = 4

--------------------------------------------------------------------------------
-- Function UseItem:Act(Unit, Param1, Param2, ItemIndex, DebugFlag) 
--
-- Accepts a unit, two selection parameters, and an item index, then uses the
-- item type to determine how to use it in the game world.
--------------------------------------------------------------------------------
function UseItem:Act(Unit, P1, P2, P3, itemIndex, debug)
    -- If the debug parameter is nil or false, set it to false.
    if not debug then
        debug = false
    end
    
    print("\tInputs:", P1, P2, P3, itemIndex)

    -- Convert the incoming item index real-value into an inventory slot
    -- number, which is in the range [0, 5] because (for whatever reason)
    -- Valve has decided to make the inventory indexes 0-based. This
    -- function does not account for the backpack because items can't
    -- be used from thos inventory locations.
    local slot = confine(itemIndex, 0, 5)

    -- Print the slot number for the item.
    print("\tSlot:", slot, itemIndex)
    
    -- Use the item index to get the item handler from the slot.
    local item = Unit:GetItemInSlot(slot)
    
    -- If we can't find an item in that slot, we return negatively.
    if not item then
        -- Print that there's no valid item in that slot.
        print("\tNo Valid Item Found")
        do return -1 end
    end
    
    -- Store the item target flags in an integer.
    local flags = item:GetBehavior()
    
    -- Get the various possible targets for deciding how to use inputs.    
    local tNone   = (bit.band(flags, DOTA_ABILITY_BEHAVIOR_NO_TARGET)   == DOTA_ABILITY_BEHAVIOR_NO_TARGET) and true or false    
    local tPoint  = (bit.band(flags, DOTA_ABILITY_BEHAVIOR_POINT)       == DOTA_ABILITY_BEHAVIOR_POINT) and true or false
    local tUnit   = (bit.band(flags, DOTA_ABILITY_BEHAVIOR_UNIT_TARGET) == DOTA_ABILITY_BEHAVIOR_UNIT_TARGET) and true or false
    
    -- Print the target status.
    print("\tCan Target:", tNone and "None" or "", tPoint and "Point" or "", tUnit and "Unit" or "")
    
    -- If the item can target nothing and there's no other targeting, attempt to just use it.
    if tNone then
        -- Print that there's no target.
        print("\tTarget: None")
        
        -- Get the unit to cast the item's ability. Why do I need the player ID? MYSTERY. 
        Unit:CastAbilityNoTarget(item, 0)
        do return end
    end
    
    -- If the item can target either a point or a unit, decide which to do here.
    if tPoint and tUnit then
        -- Convert the third parameter into a value in [0,1], then 
        -- store the equivalent true or false in the tPoint flag.
        tPoint = confine(P3) == 0 and true or false
        
        -- Make the tUnit flag the opposite of the point to make them mutually exclusive.
        tUnit  = not tPoint
    end
    
    -- If the item requires a ground target, perform that action here.
    if tPoint then               
        -- Set the first two parameters to polar values.
        local degrees  = P1
        local radius = P2
        
        -- Convert the polar coordinates into relative cartesian coordinates.
        local x, y = Dota2:ConvertToCartesianInVision(Unit, degrees, radius)
        
        -- For the purposes of clicking the ground exactly, 
        -- find the ground height at (x,y).
        local z = GetGroundHeight(Vector(x, y, 0), nil)
        
        -- Construct the target location vector.
        local location = Vector(x, y, z)
        
        -- If we're going to draw debug visuals or execute 
        -- debug code blocks, we can do so here.
        if debug then
            -- Draw a blue circle at the selected move location for 1 frame.
            -- Parameters: (center, vRgb, a, rad, ztest, duration)
            DebugDrawCircle(location, Vector(0, 0, 255), 1.0, 150, false, 1)
            
            -- Draw a blue line from the unit to the move location for 1 frame.
            -- Parameters: (origin, target, r, g, b, ztest, duration)
            DebugDrawLine(Unit:GetAbsOrigin(), location, 0, 0, 255, false, 1)
        end
        
        -- Print that we're targeting a location
        print("\tTarget: Location =", x, y)
        
        -- Cast the ability on the target location. No idea what the 0 is for.
        Unit:CastAbilityOnPosition(location, item, 0)
        
        -- Return a value of 1 to indicate we have point targeted the item.
        do return 1 end
    end
        
    -- If the item requires a unit target, perform that action here.
    if tUnit then
        -- Convert the first two parameters to the team index and the target index.
        -- The team index can also target neutrals, thus it is in [2, 4].
        local team = confine(P1, 2, 4)
        local targetIndex = P2
    
        -- Get the current unit's vision range.
        local vision = Unit:GetCurrentVisionRange()
        
        -- Convert the incoming team parameter to an integer in [2,4].
        -- This is not strong for converting to a team identifier value because it
        -- assumes uniformity of the incoming team parameter value.
        team = confine(team, 2, 4)
        
        -- Get a table of all units of the given team within the vision range
        -- of the current unit. The table is sorted by closest to farthest
        -- unit from the current unit.
        local units = FindUnitsInRadius(team,
                                        Unit:GetAbsOrigin(),
                                        nil,
                                        vision,
                                        DOTA_UNIT_TARGET_TEAM_FRIENDLY,
                                        DOTA_UNIT_TARGET_ALL,
                                        DOTA_UNIT_TARGET_FLAG_NONE,
                                        FIND_CLOSEST,
                                        false)

        -- Get the unit handler for the provided index. 
        -- Note that this method is not particularly strong for converting
        -- incoming real values into indexes, as it assumes there's some
        -- uniformity to the targetIndex parameter. If there's not, then
        -- the bias in the values will create the same bias in the indexes.
        -- Also, since the number of creeps in range of the FindUnitsInRadius
        -- function will fluctuate, the size of the table will be biased.
        local target = nil
        if units ~= nil then
            local index = confine(targetIndex, 1, size(units))
            target = units[index]
        end
        
        -- If the target is still nil, we can simply do nothing and return.
        if target == nil then
            
            do return end
        end
        
        -- If we're going to draw debug visuals or execute 
        -- debug code blocks, we can do so here.
        if debug then
            -- Get the location vector for the target unit.
            local location = target:GetAbsOrigin()
            
            -- Draw a blue circle at the selected move location for 1 frame.
            -- Parameters: (center, vRgb, a, rad, ztest, duration)
            DebugDrawCircle(location, Vector(0, 0, 255), 1.0, 150, false, 1)
            
            -- Draw a blue line from the unit to the move location for 1 frame.
            -- Parameters: (origin, target, r, g, b, ztest, duration)
            DebugDrawLine(Unit:GetAbsOrigin(), location, 0, 0, 255, false, 1)
        end
        
        -- Print that we're targeting a location
        print("\tTarget: Unit =", target:GetName())        
        
        -- Cast the ability on the target unit. No idea what the 0 is for.
        Unit:CastAbilityOnTarget(target, item, 0)   
        
        -- Return a value of 2 to indicate we have point targeted a unit.
        do return 2 end
    end
    
    -- If we get here, something didn't line up properly. Return a negative response. 
    return -100    
end

-- Return the table for this action. This becomes
-- accessible on a "require" call to this file.
return UseItem