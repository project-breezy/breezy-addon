--------------------------------------------------------------------------------
-- Bot Action: Cast on Location
--
-- Creates a table for containing the Cast on Location function and its 
-- important parameters.
--
-- The Cast on Location action is the same as a player activating a spell and 
-- left-clicking on the ground. The unit will use regular Dota 2 pathing 
-- mechanisms to get within range of the location, at which point they will
-- cast the spell on that ground location.
--
-- @classmod Action.CastOnLocation
-- @author Robert Smith
--------------------------------------------------------------------------------

-- Import any required code.
local Dota2 = require("bots/util/dota_util")
local bit   = require("bots/lib/numberlua")
require("bots/util/sys_util")

-- Create a table for holding action properties, as
-- well as the main function call logic.
local CastOnLocation = {}

-- Initialize any properties necessary for execution.
CastOnLocation.Name           = "Cast on Location"
CastOnLocation.RequiredValues = 3

--------------------------------------------------------------------------------
-- Function CastOnLocation:Act(Unit, X, Y, AbilityIndex, DebugFlag) 
--
-- Accepts a Dota unit and moves it close enough to the x, y coordinates so
-- that it can cast the spell defined by the spell index on the target location.
--------------------------------------------------------------------------------
function CastOnLocation:Act(Unit, degrees, radius, abilityIndex, debug)
    -- If the debug parameter is nil or false, set it to false.
    if not debug then
        debug = false
    end
    
    -- Create a table for holding all found abilities
    local abilities = {}

    -- Search all the unit's abilities for valid abilities and save them
    for i=0, Unit:GetAbilityCount()-1 do
        -- Grab the next ability in the unit's abilities list.
        local A = Unit:GetAbilityByIndex(i)
        
        -- We don't want to do any work if A is nil, because it will crash.
        -- Lua, very stupidly, does not have a continue keyword. Awesome.
        if A ~= nil and type(A) == "table" then
            -- As long as a player could see/use the ability and it's not
            -- passive, then we can add it to our list of valid abilities.
            if not A:IsHidden() and not A:IsPassive() then
                table.insert(abilities, A)        
            end
        end
    end
    
    -- Convert the abilityIndex parameter into a legitimate index
    -- based on the number of abilities possessed by the unit.
    abilityIndex = confine(abilityIndex, 1, size(abilities))
    
    -- Proceed two action inputs as polar coordinates to produce x,y coordinates.
    -- This will take into account the unit's vision and adjust the coordinates
    -- by the current Unit's location as an offset.
    local x, y = Dota2:ConvertToCartesianInVision(Unit, degrees, radius)
    
    -- For the purposes of clicking the ground exactly, 
    -- find the ground height at (x,y).
    local z = GetGroundHeight(Vector(x, y, 0), nil)
    
    -- Construct the target location vector.
    local location = Vector(x, y, z)
    
    -- Get the ability handler from the abilities table.
    local ability = abilities[abilityIndex]
    
    -- If we're going to draw debug visuals or execute 
    -- debug code blocks, we can do so here.
    if debug then
        -- Draw a blue circle at the selected move location for 1 frame.
        -- Parameters: (center, vRgb, a, rad, ztest, duration)
        DebugDrawCircle(location, Vector(0, 0, 255), 1.0, 150, false, 1)
        
        -- Draw a blue line from the unit to the move location for 1 frame.
        -- Parameters: (origin, target, r, g, b, ztest, duration)
        DebugDrawLine(Unit:GetAbsOrigin(), location, 0, 0, 255, false, 1)
    end
    
    -- Print the relevant parameters.
    print("\tLocation (x,y):", x, y)
    print("\tAbility Name:", ability:GetName())
    print("\tAbility State:", ability:IsFullyCastable() and "Ready" or "On Cooldown")
    
    -- If the ability is on cooldown, we don't bother trying to cast
    -- it and return negatively.
    if not ability:IsFullyCastable() then
        do return -2 end
    end
    
    -- Cast the ability on the target location. No idea what the 0 is for.
    Unit:CastAbilityOnPosition(location, ability, 0)   
end

-- Return the table for this action. This becomes
-- accessible on a "require" call to this file.
return CastOnLocation
