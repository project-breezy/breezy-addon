--------------------------------------------------------------------------------
-- Creates a table for containing the Do Nothing function and its 
-- important parameters.
--
-- The bot does nothing.
--
-- @classmod Action.DoNothing
-- @author Robert Smith
--------------------------------------------------------------------------------

-- Create a table for holding action properties, as
-- well as the main function call logic.
local DoNothing = {}

-- Initialize any properties necessary for execution.
DoNothing.Name           = "Do Nothing"
DoNothing.ParameterCount = 0

--------------------------------------------------------------------------------
-- Function DoNothing:Act()
--
-- Does nothing.
--------------------------------------------------------------------------------
function DoNothing:Act()
    -- Do nothing.
end

-- Return the table for this action. This becomes
-- accessible on a "require" call to this file.
return DoNothing