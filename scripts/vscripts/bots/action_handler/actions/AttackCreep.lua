--------------------------------------------------------------------------------
-- Creates a table for containing the Attack Creep function and its 
-- important parameters.
--
-- The Attack Creep action is the same as a player right-clicking on the 
-- creep. The unit will use regular Dota 2 pathing mechanisms to move toward
-- the creep unit until it's within the unit's attack range, at which point it
-- will proceed to continuously auto-attack the creep until cancelled or
-- either unit dies.
--
-- @classmod Action.AttackCreep
-- @author Robert Smith
--------------------------------------------------------------------------------

-- Import any required code.
require("bots/util/sys_util")

-- Create a table for holding action properties, as
-- well as the main function call logic.
local AttackCreep = {}

-- Initialize any properties necessary for execution.
AttackCreep.Name           = "Attack Creep"
AttackCreep.RequiredValues = 2

--------------------------------------------------------------------------------
-- Function AttackCreep:Act(Unit, Team, TargetIndex, DebugFlag)
--
-- Accepts a Dota unit and moves it toward the chosen creep, then auto-attacks.
--------------------------------------------------------------------------------
function AttackCreep:Act(Unit, team, targetIndex, debug)
    -- If the debug parameter is nil or false, set it to false.
    if not debug then
        debug = false
    end

    -- Get the current unit's vision range.
    local vision = Unit:GetCurrentVisionRange()
    
    -- Convert the incoming team parameter to an integer in [2, 4].
    -- This is not strong for converting to a team identifier value because it
    -- assumes uniformity of the incoming team parameter value.
    team = confine(team, 2, 4)
    
    -- Get a table of all creeps of the given team within the vision range
    -- of the current unit. The table is sorted by closest to farthest
    -- creep from the current unit.
    local creeps = FindUnitsInRadius(team,
                                     Unit:GetAbsOrigin(),
                                     nil,
                                     vision,
                                     DOTA_UNIT_TARGET_TEAM_FRIENDLY,
                                     DOTA_UNIT_TARGET_CREEP,
                                     DOTA_UNIT_TARGET_FLAG_NONE,
                                     FIND_CLOSEST,
                                     false)
    
    -- Get the unit handler for the provided index. 
    -- Note that this method is not particularly strong for converting
    -- incoming real values into indexes, as it assumes there's some
    -- uniformity to the targetIndex parameter. If there's not, then
    -- the bias in the values will create the same bias in the indexes.
    -- Also, since the number of creeps in range of the FindUnitsInRadius
    -- function will fluctuate, the size of the table will be biased.
    local target = nil
    if creeps ~= nil then
        local index = confine(targetIndex, 1, size(creeps))
        target = creeps[index]
    end
    
    -- If the target is still nil, we can simply do nothing and return.
    if target == nil then 
        print("\tNo valid target found")
        do return end
    end
    
    -- If we're going to draw debug visuals or execute 
    -- debug code blocks, we can do so here.
    if debug then
        -- Get the location vector for the target unit.
        local location = target:GetAbsOrigin()
        
        -- Draw a blue circle at the selected move location for 1 frame.
        -- Parameters: (center, vRgb, a, rad, ztest, duration)
        DebugDrawCircle(location, Vector(0, 0, 255), 1.0, 150, false, 1)
        
        -- Draw a blue line from the unit to the move location for 1 frame.
        -- Parameters: (origin, target, r, g, b, ztest, duration)
        DebugDrawLine(Unit:GetAbsOrigin(), location, 0, 0, 255, false, 1)
    end
    
    -- Print the various parameters.
    print("\tTeam  ", team)
    print("\tTarget", target:GetName())
    
    -- Move the unit toward the target and attack once in range.
    Unit:MoveToTargetToAttack(target)
end

-- Return the table for this action. This becomes
-- accessible on a "require" call to this file.
return AttackCreep
