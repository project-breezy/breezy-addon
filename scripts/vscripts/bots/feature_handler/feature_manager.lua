--------------------------------------------------------------------------------
-- Feature Handler: Basic Features
--
-- Creates a table for holding basic feature-generating functionality.
--
-- @classmod FeatureHandler
-- @author Robert Smith
--------------------------------------------------------------------------------

-- Import any necessary modules or libraries.
require("bots/util/sys_util")
local Dota2  = require("bots/util/dota_util")
local String = require("bots/util/string_util")

-- Create a table for holding action properties, as
-- well as the main function call logic.
local Features = {}

-- Initialize any properties necessary for execution.
Features.previousFeatures = {}
Features.featureCount     = 0

--------------------------------------------------------------------------------
-- Function SetFeatures(FeaturesTable, Index, Value, DefaultValue)
--
-- Copy a value into the features table at the given index. If the value
-- doesn't exist (nil or false), it will set the index to the default.
--------------------------------------------------------------------------------
function SetFeature(features, index, value, altValue)
    features[index] = value ~= nil and value or altValue
end

--------------------------------------------------------------------------------
-- Function Features:CreateFeatureSet(Unit, Addons)
--
-- Create a feature set to describe the current frame.
--------------------------------------------------------------------------------
function Features:CreateFeatureSet(Unit)
    local sTime = Time()
    local features = {}
    local opp = Dota2:GetHeroesOnTeam(DOTA_TEAM_BADGUYS)

    if size(opp) > 0 then 
        opp = opp[1]
    else 
        opp = nil 
    end

    -- Self
    
    SetFeature(features, 0, Unit:GetTeam(), "-0", 'team')
    SetFeature(features, 1, Unit:GetLevel(), -1, 'level')
    SetFeature(features, 2, Unit:GetHealth(), -2, 'health')
    SetFeature(features, 3, Unit:GetMaxHealth(), -3, 'maxHealth')
    SetFeature(features, 4, Unit:GetHealthRegen(), -4, 'healthRegen')
    SetFeature(features, 5, Unit:GetMana(), -5, 'mana')
    SetFeature(features, 6, Unit:GetMaxMana(), -6, 'maxMana')
    SetFeature(features, 7, Unit:GetManaRegen(), -7, 'manaRegen')
    SetFeature(features, 8, Unit:GetBaseMoveSpeed(), -8, 'baseMoveSpeed')
    SetFeature(features, 9, Unit:GetVelocity():Length2D(), -9, 'CurrentMoveSpeed') -- ? Replacement for getCurrentMouvmentSpeed
    SetFeature(features, 10, (Unit:GetBaseDamageMin() + Unit:GetBaseDamageMax())/2, -10, 'avgDamage') -- ? Replacement for GetBaseDamage
    SetFeature(features, 11, Dota2:GetBaseDamageVariance(Unit), -11, 'damageVar') -- ! Need replacement for Unit:GetBaseDamageVariance()
    SetFeature(features, 12, Unit:GetAttackDamage(), -12, 'attackDamage')
    SetFeature(features, 13, Unit:GetAttackRangeBuffer(), -13, 'attackRangeBuffer') -- ? Replacement for GetAttackRange()
    SetFeature(features, 14, Unit:GetAttackSpeed(), -14, 'attackSpeed')
    SetFeature(features, 15, Unit:GetSecondsPerAttack(), -15, 'secondsPerAttack')
    SetFeature(features, 16, Unit:GetAttackAnimationPoint(), -16, 'attackAnimationPoint')
    SetFeature(features, 17, Unit:GetLastAttackTime(), -17, 'attackTime')

    if Unit:GetAttackTarget() == nil then
        SetFeature(features, 18, 0, -18, 'attackTarget')
    else
        SetFeature(features, 18, String:HashCode(Unit:GetAttackTarget():GetName()), -18, 'attackTarget')
    end
    SetFeature(features, 19, Unit:GetStrength(), -19, 'strength') --? Replacement for Unit:GetAttributeValue(ATTRIBUTE_STRENGTH)
    SetFeature(features, 20, Unit:GetAgility(), -20, 'agility') -- ? Replacement for Unit:GetAttributeValue(ATTRIBUTE_AGILITY)
    SetFeature(features, 21, Unit:GetIntellect(), -21, 'intellect') -- ? Replacement for Unit:GetAttributeValue(ATTRIBUTE_INTELLECT)
    SetFeature(features, 22, Unit:GetGold(), -22, 'gold')
    SetFeature(features, 23, Dota2:GetNetWorthByUnit(Unit), -23, 'netWorth') -- ! Need replacement for Unit:GetNetWorth(), likely iterate through inventory, summing item values + getGold()
    SetFeature(features, 24, Unit:GetLastHits(), -24, 'lastHits')
    SetFeature(features, 25, Unit:GetDenies(), -25, 'denies')
    SetFeature(features, 26, Unit:GetAbsOrigin()[1], -26, 'location[1]') -- ? Replacement for Unit:GetLocation()[1]
    SetFeature(features, 27, Unit:GetAbsOrigin()[2], -27, 'location[2]') -- ? Replacement for Unit:GetLocation()[2]
    SetFeature(features, 28, Unit:GetAnglesAsVector()[2], -28, 'facing') -- ? Replacement for Unit:GetFacing()
    SetFeature(features, 29, Unit:GetCurrentVisionRange(), -29, 'visionRange')

    -- Opponent
    if opp ~= nil then
        SetFeature(features,30,opp:GetTeam(), -30, 'opp team')
        SetFeature(features,31,opp:GetLevel(), -31, 'opp level')
        SetFeature(features,32,opp:GetHealth(), -32, 'opp health')
        SetFeature(features,33,opp:GetMaxHealth(), -33, 'opp maxHealth')
        SetFeature(features,34,opp:GetHealthRegen(), -34, 'oppHealthRegen')
        SetFeature(features,35,opp:GetMana(), -35, 'oppMana')
        SetFeature(features,36,opp:GetMaxMana(), -36, 'oppMaxMana')
        SetFeature(features,37,opp:GetManaRegen(), -37, 'oppManaRegen')
        SetFeature(features,38,opp:GetBaseMoveSpeed(), -38, 'oppBaseMoveSpeed')
        SetFeature(features,39,opp:GetVelocity():Length2D(),-39,'oppCurrMoveSpeed') -- ? Replacement for getCurrentMouvmentSpeed
        SetFeature(features,40,(opp:GetBaseDamageMin() + opp:GetBaseDamageMax())/2,-40,'oppBaseDamage')
        SetFeature(features,41,Dota2:GetBaseDamageVariance(opp), -41, 'oppBaseDamageVariance') -- ! Need replacement for opp:GetBaseDamageVariance()
        SetFeature(features,42,opp:GetAttackDamage(), -42, 'oppAttackDamage')
        SetFeature(features,43,opp:GetAttackRangeBuffer(),-43, 'oppAttackRangeBuffer') -- ? Replacement for GetAttackRange()
        SetFeature(features,44,opp:GetAttackSpeed(), -44, 'oppAttackSpeed')
        SetFeature(features,45,opp:GetSecondsPerAttack(), -45, 'oppSecondsPerAttack')
        SetFeature(features,46,opp:GetAttackAnimationPoint(), -46, 'oppAttackAnimationPoint')
        SetFeature(features,47, opp:GetLastAttackTime(), -47, 'oppLastAttackTime')
        if opp:GetAttackTarget() == nil then
            SetFeature(features,48,0,-48,'oppAttackTarget')
        else
            SetFeature(features,48, String:HashCode(opp:GetAttackTarget():GetUnitName()), -48, 'oppAttackTarget')            
            --print("features[48]")
            --print(features[48])
        end
        SetFeature(features,49,opp:GetStrength(), -49, 'oppStrength')
        SetFeature(features,50,opp:GetAgility(), -50, 'oppAgility')
        SetFeature(features,51,opp:GetIntellect(), -51, 'oppIntellect')
        SetFeature(features,52,opp:GetAbsOrigin()[1], -52, 'oppLocation1')
        SetFeature(features,53,opp:GetAbsOrigin()[2], -53, 'oppLocation2')
        SetFeature(features,54,opp:GetAnglesAsVector()[2], -54, 'oppFacing') -- ? Replacement for Unit:GetFacing()
        SetFeature(features,55, opp:GetCurrentVisionRange(), -55, 'oppVisionRange')
    else
        for i=30,55 do
            SetFeature(features,i,-i,-i,'auto-fill')
        end
    end
    
    -- Match
    SetFeature(features,56, GameRules:GetDOTATime(false, false), -56, 'DOTATime' ) -- param1 = includePregameTime param2 = includeNegativeTime

    -- Towers
    local tower = Entities:FindByName(nil, "dota_goodguys_tower1_mid") -- ? Replacement for GetTower(TEAM_RADIANT, TOWER_MID_1)

    if tower ~= nil then
        SetFeature(features, 57, tower:GetTeam(), -57, "tower team")
        SetFeature(features, 58, tower:GetHealth(), -58, "tower health")
        SetFeature(features, 59, tower:GetMaxHealth(), -59, "tower max health")
        SetFeature(features, 60, tower:GetAttackDamage(), -60, "tower attack damage")
        SetFeature(features, 61, tower:GetAttackRangeBuffer(), -61, "tower attack range buffer") -- ? Replacement for GetAttackRange()
        SetFeature(features, 62, tower:GetAttackSpeed(), -62, "tower attack speed")
    else
        SetFeature(features, 57,-57,-57, "tower team")
        SetFeature(features, 58, -58, -58, "tower health")
        SetFeature(features, 59, -59, -59, "tower max health")
        SetFeature(features, 60, -60, -60, "tower attack damage")
        SetFeature(features, 61, -61, -61, "tower attack range buffer") -- ? Replacement for GetAttackRange()
        SetFeature(features, 62, -62, -62, "tower attack speed")
    end

    tower = Entities:FindByName(nil, "dota_badguys_tower1_mid") -- ? Replacement for GetTower(TEAM_DIRE, TOWER_MID_1)

    if tower ~= nil then
        SetFeature(features, 63, tower:GetTeam(), -63, "tower team")
        SetFeature(features, 64, tower:GetHealth(), -64, "tower health")
        SetFeature(features, 65, tower:GetMaxHealth(), -65, "tower max health")
        SetFeature(features, 66, tower:GetAttackDamage(), -66, "tower attack damage")
        SetFeature(features, 67, tower:GetAttackRangeBuffer(), -67, "tower attack range buffer") -- ? Replacement for GetAttackRange()
        SetFeature(features, 68, tower:GetAttackSpeed(), -68, "tower attack speed")
    else
        SetFeature(features, 63, -63, -63, "tower team")
        SetFeature(features, 64, -64, -64, "tower health")
        SetFeature(features, 65, -65, -65, "tower max health")
        SetFeature(features, 66, -66, -66, "tower attack damage")
        SetFeature(features, 67, -67, -67, "tower attack range buffer") -- ? Replacement for GetAttackRange()
        SetFeature(features, 68, -68, -68, "tower attack speed")
    end

    -- Runes
    
    local bountyRunes = Dota2:GetAllBountyRunes()
    local powerRunes  = Dota2:GetAllPowerRunes()
    
    local n = 68
    
    for i=n, 74, 2 do
        local rune    = bountyRunes and bountyRunes[i-n+1] or nil
        local runeLoc = rune and rune:GetAbsOrigin() or {}
        
        SetFeature(features,i+1,runeLoc[1],-(i+1),'bounty'..(i-n+1)..'location1')    
        SetFeature(features,i+2,runeLoc[2],-(i+2),'bounty'..(i-n+1)..'location2')
    end
    
    n = 76
    
    for i=n, 78, 2 do
        local rune    = powerRunes and powerRunes[i-n+1] or nil
        local runeLoc = rune and rune:GetAbsOrigin() or {}
        
        SetFeature(features,i+1,runeLoc[1],-(i+1),'power'..(i-n+1)..'location1')    
        SetFeature(features,i+2,runeLoc[2],-(i+2),'power'..(i-n+1)..'location2')
    end
    
    n = 80
    
    for i=n, 82, 2 do
        local rune       = powerRunes and powerRunes[i-n+1] or nil
        local runeType   = Dota2:GetRuneType(rune) or nil
        local runeStatus = Dota2:GetRuneStatusByUnit(rune)
        
        SetFeature(features,i+1,runeStatus,-(i+1),'runePowerUp'..(i-n+1)..'Type')    
        SetFeature(features,i+2,runeStatus,-(i+2),'runePowerUp'..(i-n+1)..'Status')
    end    

    -- Five Nearest Friendly Creeps

    local n = 84

    local creeps = FindUnitsInRadius(Unit:GetTeam(),
                                     Unit:GetAbsOrigin(),
                                     nil,
                                     Unit:GetCurrentVisionRange(),
                                     DOTA_UNIT_TARGET_TEAM_FRIENDLY,
                                     DOTA_UNIT_TARGET_CREEP,
                                     DOTA_UNIT_TARGET_FLAG_NONE,
                                     FIND_CLOSEST,
                                     false) 
    local nullCreeps = 0
    
    for k,v in pairs(creeps) do
        SetFeature(features, n+1, v:GetTeam(), -(n+1), 'creepTeam')
        SetFeature(features, n+2, v:GetHealth(), -(n+2), 'creepHealth')
        SetFeature(features, n+3, v:GetMaxHealth(), -(n+3), 'creepMaxHealth')
        SetFeature(features, n+4, v:GetHealthRegen(), -(n+4), 'creepHealthRegen')
        SetFeature(features, n+5, v:GetBaseMoveSpeed(), -(n+5), 'creepBaseMoveSpeed')
        SetFeature(features, n+6, v:GetVelocity():Length2D(), -(n+6), 'creepCurrentMoveSpeed')
        SetFeature(features, n+7, ((v:GetBaseDamageMin() + v:GetBaseDamageMax())/2), -(n+7), 'creepBaseDamage')
        SetFeature(features, n+8, Dota2:GetBaseDamageVariance(v), -(n+8), 'creepDamageVariance')
        SetFeature(features, n+9, v:GetAttackDamage(), -(n+9), 'creepAttackDamage')
        SetFeature(features, n+10,v:GetAttackRangeBuffer(), -(n+10), 'creepAttackRangeBuffer')
        SetFeature(features, n+11,v:GetAttackSpeed(), -(n+11), 'creepAttackSpeed')
        SetFeature(features, n+12,v:GetSecondsPerAttack(), -(n+12), 'creepSecondsPerAttack')
        SetFeature(features, n+13,v:GetAbsOrigin()[1], -(n+13), 'creepLocation1')
        SetFeature(features, n+14,v:GetAbsOrigin()[2], -(n+14), 'creepLocation2')
        n = n + 14
    end
    
    local creepCount = size(creeps)
    
    for i=n+1,n+((5-creepCount+nullCreeps)*14) do
        SetFeature(features,i,-1,-i,'autofill-creep-good')
    end
    
    n = n + ((5-creepCount+nullCreeps)*14)
    
    -- Five Nearest Enemy Creeps

    creeps = FindUnitsInRadius(Unit:GetTeam() == 2 and 3 or 2,
                               Unit:GetAbsOrigin(),
                               nil,
                               Unit:GetCurrentVisionRange(),
                               DOTA_UNIT_TARGET_TEAM_FRIENDLY,
                               DOTA_UNIT_TARGET_CREEP,
                               DOTA_UNIT_TARGET_FLAG_NONE,
                               FIND_CLOSEST,
                               false)
 
    nullCreeps = 0
    
    for k,v in pairs(creeps) do
        SetFeature(features, n+1, v:GetTeam(), -(n+1), 'creepTeam')
        SetFeature(features, n+2, v:GetHealth(), -(n+2), 'creepHealth')
        SetFeature(features, n+3, v:GetMaxHealth(), -(n+3), 'creepMaxHealth')
        SetFeature(features, n+4, v:GetHealthRegen(), -(n+4), 'creepHealthRegen')
        SetFeature(features, n+5, v:GetBaseMoveSpeed(), -(n+5), 'creepBaseMoveSpeed')
        SetFeature(features, n+6, v:GetVelocity():Length2D(), -(n+6), 'creepCurrentMoveSpeed')
        SetFeature(features, n+7, ((v:GetBaseDamageMin() + v:GetBaseDamageMax())/2), -(n+7), 'creepBaseDamage')
        SetFeature(features, n+8, Dota2:GetBaseDamageVariance(v), -(n+8), 'creepDamageVariance')
        SetFeature(features, n+9, v:GetAttackDamage(), -(n+9), 'creepAttackDamage')
        SetFeature(features, n+10,v:GetAttackRangeBuffer(), -(n+10), 'creepAttackRangeBuffer')
        SetFeature(features, n+11,v:GetAttackSpeed(), -(n+11), 'creepAttackSpeed')
        SetFeature(features, n+12,v:GetSecondsPerAttack(), -(n+12), 'creepSecondsPerAttack')
        SetFeature(features, n+13,v:GetAbsOrigin()[1], -(n+13), 'creepLocation1')
        SetFeature(features, n+14,v:GetAbsOrigin()[2], -(n+14), 'creepLocation2')
        n = n + 14
    end

    creepCount = size(creeps)
    
    for i=n+1,n+((5-creepCount+nullCreeps)*14) do
        SetFeature(features,i,-1,-i,'autofill-creep-bad')
    end
    
    n = n + ((5-creepCount+nullCreeps)*14)
    
    -- Self Abilities (6)

    local ability = nil

    for i=0,5 do
        ability = Unit:GetAbilityByIndex(i)
        SetFeature(features,n+1,ability:GetLevel(),-(n+1),'abilityLevel')
        SetFeature(features,n+2,ability:GetManaCost(ability:GetLevel()),-(n+2),'abilityManaCost') 
        SetFeature(features,n+3, ability:GetAbilityDamage(),-(n+3),'abilityDamage') 
        SetFeature(features,n+4,ability:GetCastRange(),-(n+4),'abilityCastRange') 
        SetFeature(features,n+5,ability:GetCooldownTimeRemaining(),-(n+5),'abilityCooldownTimeRemaining') 
        SetFeature(features,n+6,ability:GetAbilityTargetType(),-(n+6),'abilityTargetType') 
        SetFeature(features,n+7,bit.band(ability:GetBehavior(), DOTA_ABILITY_BEHAVIOR_AOE ),-(n+7),'abilityBehavior')  
        n = n + 7
    end

    -- Opponent Abilities (6)
    if opp ~= nil then
        for i=0,5 do
            ability = opp:GetAbilityByIndex(i)
            SetFeature(features,n+1,ability:GetLevel(),-(n+1),'abilityLevel')
            SetFeature(features,n+2,ability:GetManaCost(ability:GetLevel()),-(n+2),'abilityManaCost') 
            SetFeature(features,n+3, ability:GetAbilityDamage(),-(n+3),'abilityDamage') 
            SetFeature(features,n+4,ability:GetCastRange(),-(n+4),'abilityCastRange') 
            SetFeature(features,n+5,ability:GetCooldownTimeRemaining(),-(n+5),'abilityCooldownTimeRemaining') 
            SetFeature(features,n+6,ability:GetAbilityTargetType(),-(n+6),'abilityTargetType') 
            SetFeature(features,n+7,bit.band(ability:GetBehavior(), DOTA_ABILITY_BEHAVIOR_AOE ),-(n+7),'abilityBehavior') 
            n = n + 7
        end
    else
        for i=0,5 do
            for j=n,n+7 do
                SetFeature(features,j,-j,-j,'opponent ability autofill')
            end
            n = n + 7
        end
    end 
    
    -- Set Salve Status
    if Unit:HasItemInInventory("item_flask") == true then
        SetFeature(features,n+1,1,-(n+1), 'itemFlask')
    else
        SetFeature(features,n+1,-1,-(n+1), 'itemFlask')
    end

    local eTime = Time()
    --print("build feature vector time: " .. (eTime-sTime))
    
    -- Update the previous features table with the current features.
    self.previousFeatures = features
    self.featureCount = size(features)
    
    local features2 = {}
    
    for i=0, size(features)-1 do
        table.insert(features2, features[i])
    end
    
    -- Return the current feature set table.
    return features2
end

return Features