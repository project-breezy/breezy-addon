--------------------------------------------------------------------------------
-- The generic bot code file contains any necessary think functions and will
-- manage all of the various component classes for handling things such as
-- feature set generation, action handling, and network communication.
--
-- @classmod BotLogic
-- @author Robert Smith
--------------------------------------------------------------------------------

-- General import of any any required code.
local bit = require("bots/lib/numberlua")
require("bots/util/sys_util")

-- Create a table for containing the generic bot Think logic.
local BotLogic = {}
BotLogic.__index = BotLogic

-- Initialize any properties necessary for execution.
BotLogic.ActionHandler   = require("bots/action_handler/action_manager")
BotLogic.FeaturesHandler = require("bots/feature_handler/feature_manager")
BotLogic.HeroPurchaser   = require("bots/hero_data/nevermore")
BotLogic.HTTP            = require("bots/network/connector")
BotLogic.HeroUnit        = nil
BotLogic.HeroName        = "Shadow Fiend"
BotLogic.RelayMode        = NETWORK_MODE_SYNC
BotLogic.MatchState      = "in-progress"
BotLogic.ServerURL       = "http://localhost:8085/dota2/"
BotLogic.RadiantTeam     = {}
BotLogic.DireTeam        = {}
BotLogic.GameID          = 1
BotLogic.ThinkCount      = 0

--------------------------------------------------------------------------------
-- Accepts a table of parameters and creates a new BotLogic object.
-- 
-- @function CreateNew
-- @param parameters a table of values to be used to initialize the new object.
--------------------------------------------------------------------------------
function BotLogic:CreateNew(parameters)
    -- Create a new object. 
    local newObject = {}
    
    -- Set the default table lookup location.
    setmetatable(newObject, BotLogic)
    
    -- Initialize the new object with the input parameters table.
    newObject:Initialize(parameters)
    
    -- Return the new object.
    return newObject
end

--------------------------------------------------------------------------------
-- Accepts a table of parameters and alters the relevant class variables.
-- 
-- @function Initialize
-- @param parameters a table of values to be used to set this object's state.
--------------------------------------------------------------------------------
function BotLogic:Initialize(parameters)
    -- Set the required code for all of the handler classes.
    self.ActionHandler   = require("bots/action_handler/" .. parameters["ActionHandler"])
    self.FeaturesHandler = require("bots/feature_handler/" .. parameters["FeaturesHandler"])
    self.HeroPurchaser   = require("bots/hero_data/" .. parameters["HeroPurchaser"])
    self.HTTP            = require("bots/network/" .. parameters["HTTP"])

    -- Set the current Hero Unit handler.
    self.HeroUnit = parameters["HeroUnit"]
    
    -- Add the Hero Unit handler to the purchaser.    
    self.HeroPurchaser:SetHeroHandler(self.HeroUnit)
    
    -- Set the hero name.
    self.HeroName = self.HeroPurchaser.HeroName 
    
    -- Update the network synchronization mode.
    self.RelayMode = parameters["RelayMode"]
    
    -- Set the match state string.
    self.MatchState = parameters["MatchState"]
    
    -- Set the server URL.
    self.ServerURL = parameters["ServerURL"]
    
    -- Set the Radiant Team table.
    self.RadiantTeam = parameters["RadiantTeam"]
    
    -- Set the Dire Team table.
    self.DireTeam = parameters["DireTeam"]
    
    -- Set the Game ID.
    self.GameID = parameters["GameID"]
    
    -- Set the HTTP connector relay mode.
    self.HTTP.RelayMode = parameters["RelayMode"]
end

--------------------------------------------------------------------------------
-- Initialize the bot logic to run with a certain hero.
--
-- Accepts a table of parameters and alters the relevant class variables.
--
-- @function InitializeHero
-- @param parameters a table containing the relevant hero information.
--------------------------------------------------------------------------------
function BotLogic:InitializeHero(parameters)
       
end

--------------------------------------------------------------------------------
-- Applies the main bot logic. Automatically called by the Dota 2 client.
-- 
-- @function Think
--------------------------------------------------------------------------------
function BotLogic:Think()    
    -- Get the next action table from the network handler.
    local response = self.HTTP:CheckForNetworkResponse()
    
    -- If we receive a SYNC_WAIT, we can't proceed, so stop thinking.
    if response == NETWORK_SYNC_WAIT then
        do return end
    end
    
    -- If we have received an action, we can apply it here. 
    if response and response["actionCode"] then
        self.ActionHandler:HandleAction(self.HeroUnit, response["actionCode"], self.ThinkCount)
    end
        
    -- Attempt to purchases abilities as necessary.
    self.HeroPurchaser:BuyNextAbility()
    
    -- Attempt to purchase items as necessary.
    self.HeroPurchaser:BuyNextItem()
           
    -- Update the Think() counters
    self.ThinkCount = self.ThinkCount + 1
    
    -- Retrieve the feature set from the environment.
    local features = self.FeaturesHandler:CreateFeatureSet(self.HeroUnit)
    
    -- Create a table outlining all required JSON fields for an HTTP request.
    local httpBody = {
        gameId     = "" .. self.GameID,
        state      = self.MatchState,
        sequenceNo = "" .. self.HTTP.sequenceNumber,
        latency    = "" .. self.HTTP.lastLatency,
        features   = features           
    }

    -- Create and send the current Think()'s HTTP POST request.
    self.HTTP:SendActionRequest(httpBody, self.ServerURL)
end

-- Return the BotLogic object. If this file is imported with 
-- a `local obj = require(path)` call, it will store the object.
return BotLogic