--------------------------------------------------------------------------------
-- Creates a table for containing the network connection information and
-- functions required to handle connections to an outside agent or server.
-- 
-- @classmod Network.HTTPHandler
-- @author Robert Smith
--------------------------------------------------------------------------------

-- Import any required code
local String = require("bots/util/string_util")

-- Create a table for holding action properties, as
-- well as the main function call logic.
local HTTPHandler = {}

--- Environment constants required for execution.
NETWORK_MODE_SYNC  = "SYNC"
NETWORK_MODE_ASYNC = "ASYNC"
NETWORK_SYNC_WAIT  = "SYNC_WAIT"
NETWORK_ASYNC_WAIT = "ASYNC_WAIT"

-- Initialize any properties necessary for execution.
HTTPHandler.RelayMode      = NETWORK_MODE_SYNC
HTTPHandler.ACK            = true
HTTPHandler.droppedFrames  = 0
HTTPHandler.response       = {action=0}
HTTPHandler.lastResponse   = nil
HTTPHandler.sequenceNumber = 1
HTTPHandler.latencyTable   = {}
HTTPHandler.lastLatency    = 0
HTTPHandler.json           = require('bots/lib/dkjson')
HTTPHandler.AddonGameMode  = nil

--------------------------------------------------------------------------------
-- Check the synchronous status and return an action when able.
-- 
-- For synchronous behaviour, this function checks if the server has
-- delivered a synchronous response before proceeding to returning
-- an action. If the system has not received a response, this function
-- will return a wait response instead.
--
-- @function CheckForNetworkResponse 
-- @return   The response table if it exists, otherwise NETWORK_ASYNC_WAIT or
--           NETWORK_SYNC_WAIT depending on the relay mode.
--------------------------------------------------------------------------------
function HTTPHandler:CheckForNetworkResponse()
    -- If the network is set to be synchronous, check for acknowledgement.
    if self.RelayMode == NETWORK_MODE_SYNC then
        -- If the ACK variable has been flagged, we have received a response.
        -- If it's not flagged, we can count the number of dropped frames.
        if not self.ACK then
            -- Increment the current dropped frames.
            self.droppedFrames = self.droppedFrames + 1
                            
            -- Return a wait value while we wait.
            do return NETWORK_SYNC_WAIT end
        end
              
        -- Reset the dropped frames to 0.
        self.droppedFrames = 0
        
        -- Now that we've handled this response, set ACK to false.
        self.ACK = false
    end

    -- Store the current response as the last respose.
    self.lastResponse = self.response
    
    -- Clear the current response.
    self.response = nil
    
    -- Return the received action. 
    return self.lastResponse
end

--------------------------------------------------------------------------------
-- Send a feature set to the external server and wait for an action response.
-- 
-- When a response is received, the response is stored in the object for
-- later retrieval. If a response comes back with a discard flag as true,
-- it is dropped instead of stored.
-- 
-- @function SendActionRequest
-- @param input a string or a table representing a JSON structure.
-- @param address a string representing a server URL.
-- @return Returns true if successful.
-- @todo TODO: We should always discard a response without an action.
--------------------------------------------------------------------------------
function HTTPHandler:SendActionRequest(input, address)
    -- If we have received a table, it's probably a JSON.
    if type(input) == "table" then
        -- Decode the JSON object/table and store it as a string.
        input = self.json.encode(input)
    end
    
    -- Create an HTTP request to be sent to the given address.
    local request = CreateHTTPRequestScriptVM("POST", address)
    
    -- Set the request body to be a JSON with the provided input string.
    request:SetHTTPRequestRawPostBody("application/json", input)
    
    -- Add the current time to the latency table at the sequence number index.
    self.latencyTable[self.sequenceNumber] = Time()
    
    -- Send the completed request and implicitly wait for a response.
    -- The logic for how to handle the response is located here.
    request:Send( 
        function(result)
            -- Read the result string in as a JSON object.
            -- TODO: Breezy Server doesn't reply with a JSON yet. Change when it does.
            local json, position, error = self.json.decode(result["Body"])
            
            -- If the server tells us to discard this packet, do so.
            if json["_discard"] then
                do return end
            end
            
            -- Store the JSON object/table as the response.
            -- TODO: Breezy Server doesn't reply with a JSON yet. Change when it does.
            self.response = json
            
            -- Store the response sequence number locally for ease.
            local respSeqNum = self.response["_sequenceNo"]
            
            -- As long as there is a response and the response is not -1, proceed.
            if respSeqNum and respSeqNum ~= -1 then
                -- Update the latency table.
                self.latencyTable[respSeqNum] = Time() - self.latencyTable[respSeqNum]
                
                -- Update the previous latency value.
                self.lastLatency = self.latencyTable[respSeqNum]
                
                -- Set ACK true to let the system know it
                -- needs to resolve the received action.
                self.ACK = true
            end
        end 
    )
    
    -- Increase the sequence number by 1.
    self.sequenceNumber = self.sequenceNumber + 1
    
    -- Return true on success.
    return true
end

--------------------------------------------------------------------------------
-- Send a match start request to the external server and then wait for a
-- response containing the desired relay mode and timescale.
-- 
-- Once the response is received, the relay mode and timescale are stored
-- in the HTTPHandler object and the main addon, then applied as necessary.
-- 
-- @function SendStartRequest
-- @param input a string or a table representing a JSON structure.
-- @param address a string representing a server URL.
-- @return Returns true if successful.
--------------------------------------------------------------------------------
function HTTPHandler:SendStartRequest(input, address)
    -- If we have received a table, it's probably a JSON.
    if type(input) == "table" then
        -- Decode the JSON object/table and store it as a string.
        input = self.json.encode(input)
    end
    
    -- Create an HTTP request to be sent to the given address.
    local request = CreateHTTPRequestScriptVM("POST", address)
    
    -- Set the request body to be a JSON with the provided input string.
    request:SetHTTPRequestRawPostBody("application/json", input)

    -- Send the completed request and implicitly wait for a response.
    -- The logic for how to handle the response is located here.
    request:Send( 
        function(result)
            -- Read the result string in as a JSON object.
            -- TODO: When we update the swagger and Breezy Server, this section comes back.
            --local httpResponse, position, error = self.json.decode(result["Body"])

            -- Split the response into relevant components.
            -- TODO: When we update the swagger and Breezy Server, this section can go away.
            local response = String:Split(result["Body"], "|")
            
            -- Store the split components in the Addon Game Mode.
            -- TODO: When we update the swagger and Breezy Server, this section uses keys instead of indexes.
            self.AddonGameMode.RelayMode = response[1]
            self.AddonGameMode.Timescale = tonumber(response[2])
            
            -- Tell this connector to update its relay mode to the one received.
            self.RelayMode = self.AddonGameMode.RelayMode
            
            -- Tell the host to update its timescale.
            SendToServerConsole("host_timescale " .. self.AddonGameMode.Timescale)
        end 
    )

    -- Return true on success.
    return true
end

--------------------------------------------------------------------------------
-- Alert the external server that a match has ended.
-- 
-- @function SendEndRequest
-- @param input a string or a table representing a JSON structure.
-- @param address a string representing a server URL.
-- @return Returns true if successful.
--------------------------------------------------------------------------------
function HTTPHandler:SendEndRequest(input, address)
    -- If we have received a table, it's probably a JSON.
    if type(input) == "table" then
        -- Decode the JSON object/table and store it as a string.
        input = self.json.encode(input)
    end
    
    -- Create an HTTP request to be sent to the given address.
    local request = CreateHTTPRequestScriptVM("POST", address)
    
    -- Set the request body to be a JSON with the provided input string.
    request:SetHTTPRequestRawPostBody("application/json", input)

    -- Send the completed request and implicitly wait for a response.
    -- The logic for how to handle the response is located here.
    request:Send( 
        function(result)
            -- Do nothing when end of game is reached.
        end 
    )

    -- Return true on success.
    return true
end

--------------------------------------------------------------------------------
-- Alert the external server that the addon is waiting for a new match to start.
-- 
-- Once a response is received, this object runs a restart command on the host.
-- 
-- @function SendRestartRequest
-- @param input a string or a table representing a JSON structure.
-- @param address a string representing a server URL.
-- @return Returns true if successful.
--------------------------------------------------------------------------------
function HTTPHandler:SendRestartRequest(input, address)
    -- If we have received a table, it's probably a JSON.
    if type(input) == "table" then
        -- Decode the JSON object/table and store it as a string.
        input = self.json.encode(input)
    end
    
    -- Create an HTTP request to be sent to the given address.
    local request = CreateHTTPRequestScriptVM("POST", address)
    
    -- Set the request body to be a JSON with the provided input string.
    request:SetHTTPRequestRawPostBody("application/json", input)

    -- Send the completed request and implicitly wait for a response.
    -- The logic for how to handle the response is located here.
    request:Send( 
        function(result)
            -- Read the result string in as a JSON object.
            -- TODO: Swagger/Server update will bring this back.
            --local json, position, error = self.json.decode(result["Body"])

            -- If the server tells us to restart, do it.
            if tonumber(result["Body"]) == 1 then
                SendToServerConsole("restart")
            end
        end 
    )

    -- Return true on success.
    return true
end

--------------------------------------------------------------------------------
-- Alert the external server that the addon is waiting for a new match to start.
-- 
-- Once a response is received, this object runs a restart command on the host.
-- 
-- @function SendRestartRequest
-- @param input a string or a table representing a JSON structure.
-- @param address a string representing a server URL.
-- @return Returns true if successful.
--------------------------------------------------------------------------------
function HTTPHandler:RequestGameInformation(address)
    -- Create an HTTP request to be sent to the given address.
    local request = CreateHTTPRequestScriptVM("GET", address)

    -- Set the request body to be a JSON with the provided input string.
    request:SetHTTPRequestRawPostBody("application/json", "")
    
    -- Create a variable for storing the server game information.
    local gameInfo = nil

    -- Send the completed request and implicitly wait for a response.
    -- The logic for how to handle the response is located here.
    request:Send( 
        function(result)
            -- Decode the response's body.
            local json, position, error = self.json.decode(result["Body"])
            
            -- Store the response's body in the game info variable.
            self.AddonGameMode.ServerGameInformation = json
        end 
    )

    -- Return the game information when we receive it.
    return true
end

-- Return the table for this action. This becomes
-- accessible on a "require" call to this file.
return HTTPHandler
