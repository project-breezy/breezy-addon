First Ability Purchase:
	Purchaser State:    	{"HeroName":"Shadow Fiend","ItemsPurchased":0,"CurrentItem":"item_flask","AbilitiesPurchased":0}
	New Purchaser State:	{"LastAbility":"nevermore_shadowraze1","AbilitiesPurchased":1,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}

Start Thinking
Testing Ability Purchases:
	Level hero by 1. Current Level:	1
	Current Ability Points:	1
	Current Ability Counter:	2
	Purchaser State:	{"LastAbility":"nevermore_shadowraze1","AbilitiesPurchased":1,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_necromastery","AbilitiesPurchased":2,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	3
	New Level:	2
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	2
	Current Ability Points:	1
	Current Ability Counter:	3
	Purchaser State:	{"LastAbility":"nevermore_necromastery","AbilitiesPurchased":2,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_shadowraze1","AbilitiesPurchased":3,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	4
	New Level:	3
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	3
	Current Ability Points:	1
	Current Ability Counter:	4
	Purchaser State:	{"LastAbility":"nevermore_shadowraze1","AbilitiesPurchased":3,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_necromastery","AbilitiesPurchased":4,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	5
	New Level:	4
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	4
	Current Ability Points:	1
	Current Ability Counter:	5
	Purchaser State:	{"LastAbility":"nevermore_necromastery","AbilitiesPurchased":4,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_shadowraze1","AbilitiesPurchased":5,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	6
	New Level:	5
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	5
	Current Ability Points:	1
	Current Ability Counter:	6
	Purchaser State:	{"LastAbility":"nevermore_shadowraze1","AbilitiesPurchased":5,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_necromastery","AbilitiesPurchased":6,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	7
	New Level:	6
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	6
	Current Ability Points:	1
	Current Ability Counter:	7
	Purchaser State:	{"LastAbility":"nevermore_necromastery","AbilitiesPurchased":6,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_shadowraze1","AbilitiesPurchased":7,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	8
	New Level:	7
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	7
	Current Ability Points:	1
	Current Ability Counter:	8
	Purchaser State:	{"LastAbility":"nevermore_shadowraze1","AbilitiesPurchased":7,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_necromastery","AbilitiesPurchased":8,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	9
	New Level:	8
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	8
	Current Ability Points:	1
	Current Ability Counter:	9
	Purchaser State:	{"LastAbility":"nevermore_necromastery","AbilitiesPurchased":8,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_requiem","AbilitiesPurchased":9,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	10
	New Level:	9
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	9
	Current Ability Points:	1
	Current Ability Counter:	10
	Purchaser State:	{"LastAbility":"nevermore_requiem","AbilitiesPurchased":9,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"special_bonus_spell_amplify_6","AbilitiesPurchased":10,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	11
	New Level:	10
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	10
	Current Ability Points:	1
	Current Ability Counter:	11
	Purchaser State:	{"LastAbility":"special_bonus_spell_amplify_6","AbilitiesPurchased":10,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_dark_lord","AbilitiesPurchased":11,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	12
	New Level:	11
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	11
	Current Ability Points:	1
	Current Ability Counter:	12
	Purchaser State:	{"LastAbility":"nevermore_dark_lord","AbilitiesPurchased":11,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_requiem","AbilitiesPurchased":12,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	13
	New Level:	12
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	12
	Current Ability Points:	1
	Current Ability Counter:	13
	Purchaser State:	{"LastAbility":"nevermore_requiem","AbilitiesPurchased":12,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_dark_lord","AbilitiesPurchased":13,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	14
	New Level:	13
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	13
	Current Ability Points:	1
	Current Ability Counter:	14
	Purchaser State:	{"LastAbility":"nevermore_dark_lord","AbilitiesPurchased":13,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_dark_lord","AbilitiesPurchased":14,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	15
	New Level:	14
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	14
	Current Ability Points:	1
	Current Ability Counter:	15
	Purchaser State:	{"LastAbility":"nevermore_dark_lord","AbilitiesPurchased":14,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"special_bonus_movement_speed_25","AbilitiesPurchased":15,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	16
	New Level:	15
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	15
	Current Ability Points:	1
	Current Ability Counter:	16
	Purchaser State:	{"LastAbility":"special_bonus_movement_speed_25","AbilitiesPurchased":15,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_dark_lord","AbilitiesPurchased":16,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	17
	New Level:	16
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	16
	Current Ability Points:	0
	Current Ability Counter:	17
	Purchaser State:	{"LastAbility":"nevermore_dark_lord","AbilitiesPurchased":16,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_dark_lord","AbilitiesPurchased":16,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	17
	New Level:	17
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	17
	Current Ability Points:	1
	Current Ability Counter:	17
	Purchaser State:	{"LastAbility":"nevermore_dark_lord","AbilitiesPurchased":16,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_requiem","AbilitiesPurchased":17,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	18
	New Level:	18
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	18
	Current Ability Points:	0
	Current Ability Counter:	18
	Purchaser State:	{"LastAbility":"nevermore_requiem","AbilitiesPurchased":17,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"nevermore_requiem","AbilitiesPurchased":17,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	18
	New Level:	19
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	19
	Current Ability Points:	1
	Current Ability Counter:	18
	Purchaser State:	{"LastAbility":"nevermore_requiem","AbilitiesPurchased":17,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"special_bonus_unique_nevermore_2","AbilitiesPurchased":18,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	19
	New Level:	20
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	20
	Current Ability Points:	0
	Current Ability Counter:	19
	Purchaser State:	{"LastAbility":"special_bonus_unique_nevermore_2","AbilitiesPurchased":18,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"special_bonus_unique_nevermore_2","AbilitiesPurchased":18,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	19
	New Level:	21
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	21
	Current Ability Points:	0
	Current Ability Counter:	19
	Purchaser State:	{"LastAbility":"special_bonus_unique_nevermore_2","AbilitiesPurchased":18,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"special_bonus_unique_nevermore_2","AbilitiesPurchased":18,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	19
	New Level:	22
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	22
	Current Ability Points:	0
	Current Ability Counter:	19
	Purchaser State:	{"LastAbility":"special_bonus_unique_nevermore_2","AbilitiesPurchased":18,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"special_bonus_unique_nevermore_2","AbilitiesPurchased":18,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	19
	New Level:	23
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	23
	Current Ability Points:	0
	Current Ability Counter:	19
	Purchaser State:	{"LastAbility":"special_bonus_unique_nevermore_2","AbilitiesPurchased":18,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"special_bonus_unique_nevermore_2","AbilitiesPurchased":18,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	19
	New Level:	24
	More Abilities to Purchase?:	Yes

	Level hero by 1. Current Level:	24
	Current Ability Points:	1
	Current Ability Counter:	19
	Purchaser State:	{"LastAbility":"special_bonus_unique_nevermore_2","AbilitiesPurchased":18,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	Attempt to buy next ability.
	New Purchaser State:	{"LastAbility":"special_bonus_cooldown_reduction_30","AbilitiesPurchased":19,"CurrentItem":"item_flask","ItemsPurchased":0,"HeroName":"Shadow Fiend"}
	New Ability Counter:	20
	New Level:	25
	More Abilities to Purchase?:	No

	Ability Purchase History:
	["nevermore_shadowraze1","nevermore_necromastery","nevermore_shadowraze1","nevermore_necromastery","nevermore_shadowraze1","nevermore_necromastery","nevermore_shadowraze1","nevermore_necromastery","nevermore_requiem","special_bonus_spell_amplify_6","nevermore_dark_lord","nevermore_requiem","nevermore_dark_lord","nevermore_dark_lord","special_bonus_movement_speed_25","nevermore_dark_lord","nevermore_requiem","special_bonus_unique_nevermore_2","special_bonus_cooldown_reduction_30"]

Stop Thinking