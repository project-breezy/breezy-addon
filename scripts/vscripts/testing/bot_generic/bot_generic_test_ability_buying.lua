--------------------------------------------------------------------------------
-- Main: Generic Bot Code
--
-- The generic bot code file contains any necessary think functions and will
-- manage all of the various component classes for handling things such as
-- feature set generation, action handling, and network communication.
--
-- @author Robert Smith
--------------------------------------------------------------------------------

-- General import of any any required code.
require("bots/util/sys_util")

-- Create a table for containing the generic bot Think logic.
local BotLogic = {}
BotLogic.__index = BotLogic

-- Initialize any properties necessary for execution.
BotLogic.ActionHandler   = require("bots/action_handler/action_manager")
BotLogic.FeaturesHandler = require("bots/feature_handler/feature_manager")
BotLogic.HeroPurchaser   = require("bots/hero_data/nevermore")
BotLogic.HTTP            = require("bots/network/connector")
BotLogic.HeroUnit        = nil
BotLogic.HeroName        = "Shadow Fiend"
BotLogic.SyncMode        = NETWORK_MODE_ASYNC
BotLogic.MatchState      = "in-progress"
BotLogic.ServerURL       = "http://localhost:8085/dota2/"
BotLogic.RadiantTeam     = {}
BotLogic.DireTeam        = {}
BotLogic.ThinkCount      = 1
BotLogic.GameID          = 1

--------------------------------------------------------------------------------
-- Function BotLogic:CreateNew(Parameters)
--
-- Accepts a table of parameters and creates a new BotLogic object.
--------------------------------------------------------------------------------
function BotLogic:CreateNew(parameters)
    -- Create a new object. 
    local newObject = {}
    
    -- Set the default table lookup location.
    setmetatable(newObject, BotLogic)
    
    -- Initialize the new object with the input parameters table.
    newObject:Initialize(parameters)
    
    -- Return the new object.
    return newObject
end

--------------------------------------------------------------------------------
-- Function BotLogicCreateNew(Parameters)
--
-- Accepts a table of parameters and alters the relevant class variables.
--------------------------------------------------------------------------------
function BotLogic:Initialize(parameters)
    -- Set the required code for all of the handler classes.
    self.ActionHandler   = require("bots/action_handler/" .. parameters["ActionHandler"])
    self.FeaturesHandler = require("bots/feature_handler/" .. parameters["FeaturesHandler"])
    self.HeroPurchaser   = require("bots/hero_data/" .. parameters["HeroPurchaser"])
    self.HTTP            = require("bots/network/" .. parameters["HTTP"])
    
    -- Set the current Hero Unit handler.
    self.HeroUnit = parameters["HeroUnit"]
    
    -- Add the Hero Unit handler to the purchaser.    
    self.HeroPurchaser:SetHeroHandler(self.HeroUnit)
    
    -- Set the hero name.
    self.HeroName = self.HeroPurchaser.HeroName
        
    -- Update the network synchronization mode.
    self.SyncMode = parameters["SyncMode"]
    
    -- Set the match state string.
    self.MatchState = parameters["MatchState"]
    
    -- Set the server URL.
    self.ServerURL = parameters["ServerURL"]
    
    -- Set the Radiant Team table.
    self.RadiantTeam = parameters["RadiantTeam"]
    
    -- Set the Dire Team table.
    self.DireTeam = parameters["DireTeam"]
end


local first = true
local again = false

--------------------------------------------------------------------------------
-- Function BotLogic:Think()
--
-- Applies the main bot logic. Automatically called by the Dota 2 client.
--------------------------------------------------------------------------------
function BotLogic:Think()
    
    if first then
        print("First Ability Purchase:")
        print("\tPurchaser State:    ", json.encode(self.HeroPurchaser:GetStateAsTable()))
        self.HeroPurchaser:BuyNextAbility()
        print("\tNew Purchaser State:", json.encode(self.HeroPurchaser:GetStateAsTable()))
        print()
        first = false
        again = true
    end
    
    if again then
        local json = require("bots/lib/dkjson")
        print("Start Thinking")
        
        print("Testing Ability Purchases:")
        
        for i=2, 25 do
            print("\tLevel hero by 1. Current Level:", self.HeroUnit:GetLevel())
            self.HeroUnit:HeroLevelUp(true)
            print("\tCurrent Ability Points:", self.HeroUnit:GetAbilityPoints())
            print("\tCurrent Ability Counter:", self.HeroPurchaser.CurrentAbility)       
            print("\tPurchaser State:", json.encode(self.HeroPurchaser:GetStateAsTable()))
            print("\tAttempt to buy next ability.")
            self.HeroPurchaser:BuyNextAbility()
            print("\tNew Purchaser State:", json.encode(self.HeroPurchaser:GetStateAsTable()))
            print("\tNew Ability Counter:", self.HeroPurchaser.CurrentAbility)            
            print("\tNew Level:", self.HeroUnit:GetLevel())
            print("\tMore Abilities to Purchase?:", self.HeroPurchaser:BuyNextAbility() == 2 and "No" or "Yes")
            print()
        end
        
        print("\tAbility Purchase History:")
        print("\t" .. json.encode(self.HeroPurchaser:GetAbilityPurchaseHistory()))
        print()
        
        again = false
        print("Stop Thinking")
    end
end

-- Return the BotLogic object. If this file is imported with 
-- a `local obj = require(path)` call, it will store the object.
return BotLogic