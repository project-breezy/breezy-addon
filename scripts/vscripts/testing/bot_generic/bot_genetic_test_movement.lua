--------------------------------------------------------------------------------
-- Main: Generic Bot Code
--
-- The generic bot code file contains any necessary think functions and will
-- manage all of the various component classes for handling things such as
-- feature set generation, action handling, and network communication.
--
-- @author Robert Smith
--------------------------------------------------------------------------------

-- General import of any any required code.
require("bots/util/sys_util")

-- Create a table for containing the generic bot Think logic.
local BotLogic = {}
BotLogic.__index = BotLogic

-- Initialize any properties necessary for execution.
BotLogic.ActionHandler   = require("bots/action_handler/action_manager")
BotLogic.FeaturesHandler = require("bots/feature_handler/feature_manager")
BotLogic.HeroPurchaser   = require("bots/hero_data/nevermore")
BotLogic.HTTP            = require("bots/network/connector")
BotLogic.HeroUnit        = nil
BotLogic.HeroName        = "Shadow Fiend"
BotLogic.SyncMode        = NETWORK_MODE_ASYNC
BotLogic.MatchState      = "in-progress"
BotLogic.ServerURL       = "http://localhost:8085/dota2/"
BotLogic.RadiantTeam     = {}
BotLogic.DireTeam        = {}
BotLogic.GameID          = 1
BotLogic.ThinkCount      = 0

--------------------------------------------------------------------------------
-- Function BotLogic:CreateNew(Parameters)
--
-- Accepts a table of parameters and creates a new BotLogic object.
--------------------------------------------------------------------------------
function BotLogic:CreateNew(parameters)
    -- Create a new object. 
    local newObject = {}
    
    -- Set the default table lookup location.
    setmetatable(newObject, BotLogic)
    
    -- Initialize the new object with the input parameters table.
    newObject:Initialize(parameters)
    
    -- Return the new object.
    return newObject
end

--------------------------------------------------------------------------------
-- Function BotLogicCreateNew(Parameters)
--
-- Accepts a table of parameters and alters the relevant class variables.
--------------------------------------------------------------------------------
function BotLogic:Initialize(parameters)
    -- Set the required code for all of the handler classes.
    self.ActionHandler   = require("bots/action_handler/" .. parameters["ActionHandler"])
    self.FeaturesHandler = require("bots/feature_handler/" .. parameters["FeaturesHandler"])
    self.HeroPurchaser   = require("bots/hero_data/" .. parameters["HeroPurchaser"])
    self.HTTP            = require("bots/network/" .. parameters["HTTP"])
    
    -- Set the current Hero Unit handler.
    self.HeroUnit = parameters["HeroUnit"]
    
    -- Add the Hero Unit handler to the purchaser.    
    self.HeroPurchaser:SetHeroHandler(self.HeroUnit)
    
    -- Set the hero name.
    self.HeroName = self.HeroPurchaser.HeroName
        
    -- Update the network synchronization mode.
    self.SyncMode = parameters["SyncMode"]
    
    -- Set the match state string.
    self.MatchState = parameters["MatchState"]
    
    -- Set the server URL.
    self.ServerURL = parameters["ServerURL"]
    
    -- Set the Radiant Team table.
    self.RadiantTeam = parameters["RadiantTeam"]
    
    -- Set the Dire Team table.
    self.DireTeam = parameters["DireTeam"]
    
    -- Set the Game ID.
    self.GameID = parameters["GameID"]
end


local first = true
local again = false
local directions = {"North", "Northeast", "East", "Southeast", "South", "Southwest", "West", "Northwest"}

--------------------------------------------------------------------------------
-- Function BotLogic:Think()
--
-- Applies the main bot logic. Automatically called by the Dota 2 client.
--------------------------------------------------------------------------------
function BotLogic:Think()
    print("Start Think " .. self.ThinkCount)
    
    local think = math.floor((self.ThinkCount / 10) % 8) + 1
    
    local theta = -think * (360/8)
    local r     = 100
    
    print("Moving " .. directions[think] .. " " .. r .. " units.")
    
    print("HANDLER RETURN:", self.ActionHandler:HandleAction(self.HeroUnit, {1, theta, r, 1, 1}))
    
    -- Update the Think() counters
    self.ThinkCount = self.ThinkCount + 1
    
    print("Stop Think " .. self.ThinkCount-1)
end

-- Return the BotLogic object. If this file is imported with 
-- a `local obj = require(path)` call, it will store the object.
return BotLogic