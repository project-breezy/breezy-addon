---------------------------------------------------------------------------------------------
-- This is the main addon file for the Breezy Addon. It contains all of the information
-- and code necessary to start the game mode. More information can be found below.
--
-- Notes:
-- 
-- Rune Spawning Rules:
--      1) Bounty Runes spawn at 0:00 on the game clock, and every five minutes after that.
--      2) Power-up Runes spawn at 4:00 on the game clock, and every two minutes after that.
--      3) Bounty Runes spawn at four locations on the map, one on each of the secondary 
--         jungles of both factions, and two in the river which are contested.
--      4) Power-up Runes spawn randomly at one of the two locations in the river.
--      5) The same Power-up Rune cannot spawn twice in a row.
--      6) Unused runes will be replaced by a new rune at the next spawn time.
--
-- @classmod ProjectBreezyGameMode
-- @author Robert Smith
---------------------------------------------------------------------------------------------

--TODO: Remove the string utility when the split is no longer required for HTTP responses.
local String = require("bots/util/string_util")

-- Initialize the main class objects for this addon.
local ProjectBreezyGameMode = class({})

-- Initialize any properties necessary for execution.
ProjectBreezyGameMode.BotLogic              = require("bots/bot_generic")
ProjectBreezyGameMode.HeroUnit              = nil
ProjectBreezyGameMode.Timescale             = 1
ProjectBreezyGameMode.RelayMode             = "SYNC"
ProjectBreezyGameMode.HTTP                  = require("bots/network/connector")
ProjectBreezyGameMode.RadiantInitialized    = false
ProjectBreezyGameMode.DireInitialized       = false
ProjectBreezyGameMode.ServerURL             = "http://localhost:8085"
ProjectBreezyGameMode.GameOver              = false
ProjectBreezyGameMode.GameID                = -1
ProjectBreezyGameMode.ServerGameInformation = nil
ProjectBreezyGameMode.SelectionStarted      = false
ProjectBreezyGameMode.RequestedHero         = "invoker"
ProjectBreezyGameMode.WinConditions         = require("bots/end_of_game_handler/win_conditions")

---------------------------------------------------------------------------------------------
-- This function is required for this addon to run, but does nothing for us.
-- 
-- @function Precache
-- @param context information on the context of the game client.
---------------------------------------------------------------------------------------------
function Precache(context)
    -- We don't need to precache anything.
end

---------------------------------------------------------------------------------------------
-- This is called automatically by the Dota 2 client when the addon is first loaded. 
-- We have to initialize our Addon Template object and also initialize the game mode.
-- 
-- From here (or below) is where we register the global Game Mode Think function (which
-- is called every frame) to handle the game conditions from frame-to-frame as the
-- game is processing and being played. If we don't register a Game Mode Think function,
-- then this addon technically won't do anything besides exist as static data.
-- 
-- @function Activate
---------------------------------------------------------------------------------------------
function Activate()
    -- Register the main addon template.
    GameRules.AddonTemplate = ProjectBreezyGameMode()
    
    -- Create a table of parameters for the addon template.
    local parameters = {
        BotLogic           = "bot_generic",
        Timescale          = 1,
        RelayMode          = "SYNC",
        HTTP               = "connector",
        RadiantInitialized = false,
        DireInitialized    = false,
        ServerURL          = "http://localhost:8085",
        WinConditions      = "win_conditions"
    }
    
    -- Initialize the game mode object.
    GameRules.AddonTemplate:Initialize(parameters)
        
    -- Set up the addon's match parameters.
    GameRules.AddonTemplate:SetUpGameParameters()
end

---------------------------------------------------------------------------------------------
-- Initialize a ProjectBreezyGameMode object with new class variables.
-- 
-- @function Initialize
-- @param parameters a table containing the parameters to be stored in the object.
---------------------------------------------------------------------------------------------
function ProjectBreezyGameMode:Initialize(parameters)
    -- Initialize the bot logic.
    self.BotLogic = require("bots/" .. parameters["BotLogic"])
    
    -- Set the timescale.
    self.Timescale = parameters["Timescale"]
    
    -- Set the relay mode.
    self.RelayMode = parameters["RelayMode"]
    
    -- Create an HTTP POST request handler.
    self.HTTP = require("bots/network/" .. parameters["HTTP"])
    
    -- Give this object to the HTTP connector in case it needs to update values.
    self.HTTP.AddonGameMode = self
    
    -- Set the Radiant team initialization flag.
    self.RadiantInitialized = parameters["RadiantInitialized"]
    
    -- Set the Dire team initialization flag.
    self.DireInitialized = parameters["DireInitialized"]
    
    -- Set the base external server URL.
    self.ServerURL = parameters["ServerURL"]
    
    -- Set the win conditions handler.
    self.WinConditions = require("bots/end_of_game_handler/" .. parameters["WinConditions"])
end

---------------------------------------------------------------------------------------------
-- This function is called after we create and store the Project Breezy Game Mode object.
-- 
-- In this function, we set all of the game parameters, such as time limits and any
-- custom spawning filters for runes or units. Once this function is complete, the 
-- data boundaries of the game are initialized.
-- 
-- @function SetUpGameParameters
---------------------------------------------------------------------------------------------
function ProjectBreezyGameMode:SetUpGameParameters()
    -- Extract the game mode entity from the game rules.
    local mode = GameRules:GetGameModeEntity()
    
    -- Set any game mode flags.
    mode:SetUseDefaultDOTARuneSpawnLogic(true)
    mode:SetFogOfWarDisabled(true)

    -- Set the Game Mode Think function. This will be called once per frame.
    -- The think function will not start processing for two seconds.
    GameRules:GetGameModeEntity():SetThink( "OnThink", self, "GameModeThink", 2 )
    
    -- Set any game rules flags.
    GameRules:GetGameModeEntity():SetFreeCourierModeEnabled(true)
    GameRules:GetGameModeEntity():SetStashPurchasingDisabled(false)
    GameRules:SetCustomGameSetupRemainingTime(0)
    GameRules:EnableCustomGameSetupAutoLaunch(true)
    GameRules:SetPreGameTime(5)
    GameRules:SetShowcaseTime(0)
    GameRules:SetCustomGameSetupAutoLaunchDelay(0)
    GameRules:SetHeroSelectionTime(0)
    GameRules:SetSameHeroSelectionEnabled(true)
    GameRules:SetStrategyTime(0)
    
    -- Execute any necessary console commands.
    SendToServerConsole("r_always_render_all_windows 1")
    SendToServerConsole("engine_no_focus_sleep_vconsole_suppress 1")
    SendToServerConsole("r_vconsole_foregroundforcerender 1")        
end

---------------------------------------------------------------------------------------------
-- This function is called after the game starts to initialize all entities and data.
-- 
-- In this function, we set the initial timescale, report to the server to let it know
-- the game has actually started, configure any relevant entities, initialize the Bot
-- Logic object, then register the bot Think() function.
--  
-- @function SetUpGameParameters
---------------------------------------------------------------------------------------------
function ProjectBreezyGameMode:SetUpGameContent()
    -- Save the current unit.
    -- TODO: This should be able to choose a specific player to capture instead of 0.
    self.HeroUnit = PlayerResource:GetPlayer(0):GetAssignedHero()
    
    -- If we didn't receive a hero unit, we can't finish this process.
    if not self.HeroUnit then
        do return end
    end
    
    -- Set the default timescale.
    SendToServerConsole("host_timescale 10")
    
    -- Generate a pseudo-unique ID for this match.    
    self.GameID = GetSystemTime() .. tostring(math.random(9999))

    -- Create a table outlining all required JSON fields for an HTTP request.
    local httpBody = {
        gameId = "" .. self.GameID,
        state  = "start"           
    }
    
    -- Create and send the current match start server request. This will
    -- produce the requested Timescale and Relay Mode and update it
    -- automatically in the connector object, so we don't have to wait.
    self.HTTP:SendStartRequest(httpBody, self.ServerURL .. "/run/client")

    -- Remove top and bottom creep spawners.
    Entities:FindByClassname(nil, "npc_dota_spawner_good_top"):RemoveSelf()
    Entities:FindByClassname(nil, "npc_dota_spawner_good_bot"):RemoveSelf()
    Entities:FindByClassname(nil,  "npc_dota_spawner_bad_top"):RemoveSelf()
    Entities:FindByClassname(nil,  "npc_dota_spawner_bad_bot"):RemoveSelf()
    
    -- Set all of the non-mid towers to be invulnerable.    
    Entities:FindByName(nil, "dota_goodguys_tower1_top"):SetInvulnCount(1)
    Entities:FindByName(nil, "dota_goodguys_tower1_bot"):SetInvulnCount(1)
    Entities:FindByName(nil,  "dota_badguys_tower1_top"):SetInvulnCount(1)
    Entities:FindByName(nil,  "dota_badguys_tower1_bot"):SetInvulnCount(1)

    -- Create a table of bot logic initialization parameters.
    local parameters = {
        ActionHandler   = "action_manager",
        FeaturesHandler = "feature_manager",
        HeroPurchaser   = self.RequestedHero,
        HTTP            = "connector",
        
        HeroUnit        = self.HeroUnit,
        RelayMode       = self.RelayMode,
        MatchState      = "in-progress",
        ServerURL       = "http://localhost:8085/dota2/",
        RadiantTeam     = { self.HeroUnit },
        DireTeam        = {},
        GameID          = self.GameID
    }
    
    -- Initialize the Bot Logic object.
    self.BotLogic:Initialize(parameters)
    
    -- Set the hero unit's think function and set it to run every frame.
    self.HeroUnit:SetContextThink("playDota", function()
        self.BotLogic:Think()
        return 0
    end, 0)

    -- Enable the bots to run their think functions as necessary.
    GameRules:GetGameModeEntity():SetBotThinkingEnabled(true)
    
    -- We have completed initializing the main unit congfiguration.
    self.RadiantInitialized = true
    
    -- Center the camera on this instance's main hero.
    SendToServerConsole("+dota_camera_center_on_hero")
end

---------------------------------------------------------------------------------------------
-- Update the state of the game once per frame with this function.
-- 
-- This think function is registered to the addon and is set to be called continuously
-- until the game ends. It contains information on how to manage events in the game
-- throughout various game states (pre-game, hero selection, etc.). 
-- 
-- This function also contains conditions under which a match ends in victory. When the
-- victory conditions are met on a given frame, this function will end the game.
-- 
-- @function OnThink()
---------------------------------------------------------------------------------------------
function ProjectBreezyGameMode:OnThink()    
    --**************************
    -- START IN-GAME LOGIC
    --**************************
    if GameRules:State_Get() == DOTA_GAMERULES_STATE_PRE_GAME or GameRules:State_Get() == DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
        -- If we haven't initialized, but the heroes have spawned, we run this code.
        if not self.RadiantInitialized then
            self:SetUpGameContent()
        end
        
        -- Check the win conditions to see if someone has won.
        self.WinConditions:CheckForVictory()

    --**************************
    -- START POST GAME LOGIC
    --**************************
    elseif GameRules:State_Get() == DOTA_GAMERULES_STATE_POST_GAME then
        -- Reset the current hero's think function.
        self.HeroUnit:SetContextThink("playDota", function()
            return
        end, 0)
        
        -- Create the end-of-game state request body.
        -- TODO: Stop sending strings. Send numbers and update the specification.
        local httpBody = {
            gameId       = "" .. self.GameID,
            state        = "end",
            winner       = "" .. self.WinConditions.Winner,
            direKills    = "" .. self.WinConditions.DireKills,
            radiantKills = "" .. self.WinConditions.RadiantKills,
            deaths       = "" .. PlayerResource:GetDeaths(0)            
        } 
        
        -- Send an end-of-game notification.
        self.HTTP:SendEndRequest(httpBody, self.ServerURL .. "/run/client")
        
        -- Move the game state to custom game setup so we can restart.
        GameRules:ResetToCustomGameSetup()

    --*******************************
    -- START HERO SELECTION LOGIC
    --*******************************    
    elseif GameRules:State_Get() == DOTA_GAMERULES_STATE_HERO_SELECTION then    
        -- If we haven't gotten the server game information, we run this code once.
        -- We cannot select a hero until this HTTP request is fulfilled.
        if not self.SelectionStarted then
            -- Send a request for game information from the server
            self.HTTP:RequestGameInformation(self.ServerURL .. "/run/active")
            
            -- Add the enemy bots to the game here.
            -- TODO: Add the ability to add multiple bots as needed.
            -- TODO: Add the ability to control difficulty.
            -- TODO: Add the ability to select different heroes.
            Tutorial:AddBot("npc_dota_hero_nevermore", "mid", "hard", false)

            -- Alert the system that selection has been initiated.
            -- This will stop the code from running twice.
            self.SelectionStarted = true
        end
        
        -- If we don't have the game information from the server, we can't select a hero.
        if not self.ServerGameInformation then
            do return 1 end
        end
        -- Retrieve the name of the requested hero.
        self.RequestedHero = self.ServerGameInformation["gameConfig"]["agentHero"]
        self.RequestedHero = string.sub(self.RequestedHero, 15)
               
        -- Select the hero for the learning agent.
        -- TODO: Add the ability to add multiple bots as needed.                
        PlayerResource:GetPlayer(0):SetSelectedHero(self.ServerGameInformation["gameConfig"]["agentHero"])
        
        -- Tell the client that the custom game setup is complete and the match can start.
        GameRules:FinishCustomGameSetup() 
    
    --*********************************
    -- START GAME PRE-RESTART LOGIC
    --*********************************
    elseif GameRules:State_Get() == DOTA_GAMERULES_STATE_CUSTOM_GAME_SETUP  then
        -- Create a table outlining all required JSON fields for an HTTP request.
        local httpBody = {
            state      = "waiting"
        }
        
        -- Send a waiting-for-restart notification. This will repeat until successful.
        self.HTTP:SendRestartRequest(httpBody, self.ServerURL .. "/run/client")
    end
    
    -- Center the camera on this instance's main hero.
    SendToServerConsole("+dota_camera_center_on_hero")
    
    -- Return 1 on a successful Think() end.
    return 1
end
